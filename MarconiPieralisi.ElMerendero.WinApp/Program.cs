﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon
using System.Text.RegularExpressions;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    static class Program
    {
        //Path accesso webservice
        internal static ClsCredenziali credenziali;
        //public const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws3/";
        public const string WSPATH = "http://iismarconipieralisi.sviluppo.host/ws3/";
        //public const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws2/";
        //public const string WSPATH = "http://elmerendero.infinityfreeapp.com/ws3/";

        //Forms statici MDI Container
        public static FrmClassi FrmClassi;
        public static FrmOrdine FrmOrdine;
        public static FrmOrdini FrmOrdini;
        public static FrmProdotti FrmProdotti;
        public static FrmUtentiAdm FrmUtentiAdmin;
        public static FrmCategoria FrmCategoria;
        public static FrmPuntoVendita FrmPuntoVendita;
        public static FrmSetup FrmSetup;

        public static int idUtente;
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new FrmLogin());
            Application.Run(new FrmSplashScreen());

        }
        
        public static string AggiungiCredenziali(string oggetto)
        {
            //ClsCredenziali credenziale
            string s = JsonConvert.SerializeObject( credenziali, Formatting.Indented);
            string body ="{​​\"user\" : ​​" + s + ",\"object\" : " + oggetto + " }​​";

            body = Regex.Replace(body, @"[^\u0009\u000A\u000D\u0020-\u007E]", string.Empty);
            
            
            return body;

        }
        public static void SetCredenziali(string email, string token)
        {
            credenziali = new ClsCredenziali();
            credenziali.Email = email;
            credenziali.Token = token;
        }
      
    }
}
