﻿
namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmProdotti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProdotti));
            this.label4 = new System.Windows.Forms.Label();
            this.txtdescrizione = new System.Windows.Forms.TextBox();
            this.txtnomeprodotto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblquantità = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnsalvautente = new System.Windows.Forms.Button();
            this.btnaggiorna = new System.Windows.Forms.Button();
            this.lsvprodotti = new System.Windows.Forms.ListView();
            this.tbnome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbquantitàMax = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbdescrizione = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbprezzo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbcategoria = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnelimina = new System.Windows.Forms.Button();
            this.btnmodifica = new System.Windows.Forms.Button();
            this.btnaggiungi = new System.Windows.Forms.Button();
            this.numQuantitàMax = new System.Windows.Forms.NumericUpDown();
            this.numPrezzo = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.numQuantita = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCategoria = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantitàMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrezzo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantita)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(706, 173);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 19);
            this.label4.TabIndex = 90;
            this.label4.Text = "Prezzo";
            // 
            // txtdescrizione
            // 
            this.txtdescrizione.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescrizione.Location = new System.Drawing.Point(786, 144);
            this.txtdescrizione.Name = "txtdescrizione";
            this.txtdescrizione.Size = new System.Drawing.Size(100, 22);
            this.txtdescrizione.TabIndex = 89;
            // 
            // txtnomeprodotto
            // 
            this.txtnomeprodotto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnomeprodotto.Location = new System.Drawing.Point(786, 116);
            this.txtnomeprodotto.Name = "txtnomeprodotto";
            this.txtnomeprodotto.Size = new System.Drawing.Size(100, 22);
            this.txtnomeprodotto.TabIndex = 87;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(669, 145);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 19);
            this.label3.TabIndex = 86;
            this.label3.Text = "Descrizione";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(715, 116);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 19);
            this.label2.TabIndex = 85;
            this.label2.Text = "Nome";
            // 
            // lblquantità
            // 
            this.lblquantità.AutoSize = true;
            this.lblquantità.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblquantità.Location = new System.Drawing.Point(580, 225);
            this.lblquantità.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblquantità.Name = "lblquantità";
            this.lblquantità.Size = new System.Drawing.Size(189, 19);
            this.lblquantità.TabIndex = 84;
            this.lblquantità.Text = "Quantità max ordinabile";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(853, 280);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 19);
            this.label5.TabIndex = 83;
            this.label5.Text = "Salva";
            // 
            // btnsalvautente
            // 
            this.btnsalvautente.BackColor = System.Drawing.Color.Transparent;
            this.btnsalvautente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalvautente.BackgroundImage")));
            this.btnsalvautente.Location = new System.Drawing.Point(816, 267);
            this.btnsalvautente.Name = "btnsalvautente";
            this.btnsalvautente.Size = new System.Drawing.Size(32, 32);
            this.btnsalvautente.TabIndex = 82;
            this.btnsalvautente.Text = " ";
            this.btnsalvautente.UseVisualStyleBackColor = false;
            this.btnsalvautente.Click += new System.EventHandler(this.btnsalvautente_Click_1);
            // 
            // btnaggiorna
            // 
            this.btnaggiorna.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiorna.Location = new System.Drawing.Point(467, 82);
            this.btnaggiorna.Name = "btnaggiorna";
            this.btnaggiorna.Size = new System.Drawing.Size(95, 33);
            this.btnaggiorna.TabIndex = 81;
            this.btnaggiorna.Text = "Aggiorna";
            this.btnaggiorna.UseVisualStyleBackColor = true;
            this.btnaggiorna.Click += new System.EventHandler(this.btnaggiorna_Click);
            // 
            // lsvprodotti
            // 
            this.lsvprodotti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tbnome,
            this.tbquantitàMax,
            this.tbdescrizione,
            this.tbprezzo,
            this.tbcategoria});
            this.lsvprodotti.FullRowSelect = true;
            this.lsvprodotti.HideSelection = false;
            this.lsvprodotti.Location = new System.Drawing.Point(12, 82);
            this.lsvprodotti.Name = "lsvprodotti";
            this.lsvprodotti.Size = new System.Drawing.Size(420, 330);
            this.lsvprodotti.TabIndex = 77;
            this.lsvprodotti.UseCompatibleStateImageBehavior = false;
            this.lsvprodotti.View = System.Windows.Forms.View.Details;
            this.lsvprodotti.SelectedIndexChanged += new System.EventHandler(this.lsvprodotti_SelectedIndexChanged);
            // 
            // tbnome
            // 
            this.tbnome.Text = "Nome";
            this.tbnome.Width = 90;
            // 
            // tbquantitàMax
            // 
            this.tbquantitàMax.Text = "Quantità massima";
            this.tbquantitàMax.Width = 102;
            // 
            // tbdescrizione
            // 
            this.tbdescrizione.Text = "Descrizione";
            this.tbdescrizione.Width = 115;
            // 
            // tbprezzo
            // 
            this.tbprezzo.Text = "Prezzo";
            this.tbprezzo.Width = 49;
            // 
            // tbcategoria
            // 
            this.tbcategoria.Text = "Categoria";
            // 
            // btnelimina
            // 
            this.btnelimina.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelimina.Location = new System.Drawing.Point(467, 216);
            this.btnelimina.Name = "btnelimina";
            this.btnelimina.Size = new System.Drawing.Size(95, 33);
            this.btnelimina.TabIndex = 80;
            this.btnelimina.Text = "Elimina";
            this.btnelimina.UseVisualStyleBackColor = true;
            this.btnelimina.Click += new System.EventHandler(this.btnelimina_Click);
            // 
            // btnmodifica
            // 
            this.btnmodifica.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodifica.Location = new System.Drawing.Point(467, 177);
            this.btnmodifica.Name = "btnmodifica";
            this.btnmodifica.Size = new System.Drawing.Size(95, 33);
            this.btnmodifica.TabIndex = 79;
            this.btnmodifica.Text = "Modifica";
            this.btnmodifica.UseVisualStyleBackColor = true;
            this.btnmodifica.Click += new System.EventHandler(this.btnmodifica_Click);
            // 
            // btnaggiungi
            // 
            this.btnaggiungi.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiungi.Location = new System.Drawing.Point(467, 138);
            this.btnaggiungi.Name = "btnaggiungi";
            this.btnaggiungi.Size = new System.Drawing.Size(95, 33);
            this.btnaggiungi.TabIndex = 78;
            this.btnaggiungi.Text = "Aggiungi";
            this.btnaggiungi.UseVisualStyleBackColor = true;
            this.btnaggiungi.Click += new System.EventHandler(this.btnaggiungi_Click);
            // 
            // numQuantitàMax
            // 
            this.numQuantitàMax.Location = new System.Drawing.Point(786, 224);
            this.numQuantitàMax.Name = "numQuantitàMax";
            this.numQuantitàMax.Size = new System.Drawing.Size(120, 20);
            this.numQuantitàMax.TabIndex = 92;
            // 
            // numPrezzo
            // 
            this.numPrezzo.DecimalPlaces = 2;
            this.numPrezzo.Location = new System.Drawing.Point(786, 172);
            this.numPrezzo.Name = "numPrezzo";
            this.numPrezzo.Size = new System.Drawing.Size(120, 20);
            this.numPrezzo.TabIndex = 93;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(686, 88);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 19);
            this.label1.TabIndex = 96;
            this.label1.Text = "Categoria";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::MarconiPieralisi.ElMerendero.WinApp.Properties.Resources.prodotti_nero;
            this.pictureBox4.Location = new System.Drawing.Point(12, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(143, 60);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 97;
            this.pictureBox4.TabStop = false;
            // 
            // numQuantita
            // 
            this.numQuantita.Location = new System.Drawing.Point(785, 198);
            this.numQuantita.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numQuantita.Name = "numQuantita";
            this.numQuantita.Size = new System.Drawing.Size(120, 20);
            this.numQuantita.TabIndex = 98;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(608, 196);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 19);
            this.label6.TabIndex = 99;
            this.label6.Text = "Quantità disponibile";
            // 
            // cmbCategoria
            // 
            this.cmbCategoria.FormattingEnabled = true;
            this.cmbCategoria.Location = new System.Drawing.Point(785, 89);
            this.cmbCategoria.Name = "cmbCategoria";
            this.cmbCategoria.Size = new System.Drawing.Size(121, 21);
            this.cmbCategoria.TabIndex = 100;
            // 
            // FrmProdotti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 424);
            this.Controls.Add(this.cmbCategoria);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numQuantita);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numPrezzo);
            this.Controls.Add(this.numQuantitàMax);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtdescrizione);
            this.Controls.Add(this.txtnomeprodotto);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblquantità);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnsalvautente);
            this.Controls.Add(this.btnaggiorna);
            this.Controls.Add(this.lsvprodotti);
            this.Controls.Add(this.btnelimina);
            this.Controls.Add(this.btnmodifica);
            this.Controls.Add(this.btnaggiungi);
            this.Name = "FrmProdotti";
            this.Text = "frmprodotti";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmProdotti_FormClosing);
            this.Load += new System.EventHandler(this.FrmProdotti_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numQuantitàMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrezzo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantita)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtdescrizione;
        private System.Windows.Forms.TextBox txtnomeprodotto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblquantità;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnsalvautente;
        private System.Windows.Forms.Button btnaggiorna;
        private System.Windows.Forms.ListView lsvprodotti;
        private System.Windows.Forms.ColumnHeader tbnome;
        private System.Windows.Forms.ColumnHeader tbquantitàMax;
        private System.Windows.Forms.ColumnHeader tbdescrizione;
        private System.Windows.Forms.ColumnHeader tbprezzo;
        private System.Windows.Forms.Button btnelimina;
        private System.Windows.Forms.Button btnmodifica;
        private System.Windows.Forms.Button btnaggiungi;
        private System.Windows.Forms.NumericUpDown numQuantitàMax;
        private System.Windows.Forms.NumericUpDown numPrezzo;
        private System.Windows.Forms.ColumnHeader tbcategoria;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.NumericUpDown numQuantita;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbCategoria;
    }
}