﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmOrdine : Form
    {
        #region VARIABILI GLOBALI
        List<ClsProdotto> prodotti = new List<ClsProdotto>();
        ClsProdotto prodotto = new ClsProdotto();
        ClsOrdini ordine = new ClsOrdini();
        //bool salvaOmodifica;
        //bool modifica = false;
        //int quantitaBase = 1;
        double prezzo = 0;
        int noQuantita = 0;
        bool edit = false;
        #endregion

        public FrmOrdine()
        {
            InitializeComponent();
        }

        #region METODI

        //public void PassaOrdine(ClsOrdini Ordin)
        //{
        //    ordine = Ordin;
        //    MessageBox.Show("funziono");
        //}

        private void aggiornamax()
        {
            if (lsvprodotti.FocusedItem != null) {
                //leggo tutti i prodotti e prendo quello che ha l'id uguale a quello selezionato, 
                //poi metto il massimo della nud uguale alla quantita max del prodotto.
                List<ClsProdotto> prodotti = new ClsProdotto().getProdotti();
                int idprodotto = Convert.ToInt32(lsvprodotti.SelectedItems[0].Tag);
                ClsProdotto prod = prodotti.Find(app => app.ID == idprodotto);
                udQuantita.Maximum = prod.quantitamax;
            }

                
        }

        private void loadProdotti()
        {


            ClsProdotto prodotto = new ClsProdotto();
            prodotti = prodotto.getProdotti();

            lsvprodotti.Items.Clear();
            foreach (var item in prodotti)
            {
                ListViewItem lvi = new ListViewItem(item.nome);
                lvi.SubItems.Add(item.prezzo.ToString());
                lvi.SubItems.Add(item.quantita.ToString());

                lsvprodotti.Items.Add(lvi);
            }
        }

        private void loadCategorie()
        {


            ClsCategoria categoria = new ClsCategoria();
            List<ClsCategoria> categorie = categoria.getCategorie();
            int x = 310, y = 280;
            foreach (ClsCategoria cat in categorie)
            {
                Button btn = new Button();
                btn.Text = cat.nome;
                btn.Size = new Size(100, 50);
                btn.Location = new Point(x, y);
                y += 60;
                this.Controls.Add(btn);
            }

        }

        private void Aggiorna(ClsProdotto prodotto)
        {
            prodotti = prodotto.getProdotti();
            lsvCarrello.Items.Clear();
            if (prodotti != null)
            {
                foreach (ClsProdotto item in prodotti)
                {
                    ListViewItem lvi = new ListViewItem(item.nome);
                    lvi.SubItems.Add(item.prezzo.ToString());
                    lvi.SubItems.Add(item.quantita.ToString());
                    lsvCarrello.Items.Add(lvi);
                }
            }
        }
        #endregion

        #region EVENTI

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void FrmUtente_Load(object sender, EventArgs e)
        {
            //user control
            //ucbuttons ucb = new ucbuttons();
            //ucb.dock = dockstyle.left; // per riempire lo spazio di sinistra
            //ucb.bringtofront();  // per portare i bottoni in primo piano
            //this.controls.add(ucb); // per aggiungere lo usercontrol alla form
            //panel2.sendtoback(); // per assicurarsi che lo usercontrol non finisca sopra al panello del titolo

            //carico le categorie dei prodotti
            /*Button bt1 = new Button();
            bt1.Name = "btPizza"; // In realtà il valore è dinamico preso dal foreach come già realizzato
            bt1.Tag = 3; // Qui inserisci l'ID della categoria del prodotto
            bt1.Text = "Pizza"; // Qui inserisci il nome del prodotto
            bt1.Location = new Point(X, Y);
            bt1.Click += new System.EventHandler(this.bt1_Click);
            this.Controls.Add(bt1);
            loadCategorie(); */


            ClsCategoria categoria = new ClsCategoria();
            List<ClsCategoria> categorie = categoria.getCategorie();

            int pos = 116;
            int IDbottone = 1;
            if (categorie != null)
            {
                foreach (ClsCategoria item in categorie)
                {
                    Button btn = new Button();
                    this.Controls.Add(btn);
                    btn.Text = item.nome; // il testo dell bottone
                    btn.Name = "btn" + IDbottone.ToString(); //ID del bottone
                    btn.Click += new EventHandler(this.btn_Click); //evento bottone
                    btn.Size = new Size(80, 33);// la dimensione del bottone
                    btn.Location = new System.Drawing.Point(120, pos);// la posizione del bottone
                    btn.Visible = true;
                    pos = pos + 50;
                    IDbottone++;
                }
            }
        }

        void btn_Click(object sender, EventArgs e)
        {

            // Codice per caricare i prodotti della categoria del bottone (preleva l'ID dal tag)
            Button btn = sender as Button;
            if (btn.Name == "btn1")
            {
                ClsProdotto prodotto = new ClsProdotto();
                prodotti = prodotto.getProdotti();

                if (prodotti != null)
                {
                    lsvprodotti.Items.Clear();
                    foreach (var item in prodotti)
                    {
                        if (item.IDCategoria == 1)
                        {
                            ListViewItem lvi = new ListViewItem(item.nome);
                            lvi.SubItems.Add(item.prezzo.ToString());
                            lvi.SubItems.Add(item.quantita.ToString());

                            lvi.Tag = item.ID;
                            lsvprodotti.Items.Add(lvi);
                        }
                    }
                    rimuoviquantitacarrello();
                }
                //MessageBox.Show("Categoria " + btn.Text + " selezionata");
            }
            if (btn.Name == "btn2")
            {
                ClsProdotto prodotto = new ClsProdotto();
                prodotti = prodotto.getProdotti();
                if (prodotti != null)
                {
                    lsvprodotti.Items.Clear();
                    foreach (var item in prodotti)
                    {
                        if (item.IDCategoria == 2)
                        {
                            ListViewItem lvi = new ListViewItem(item.nome);
                            lvi.SubItems.Add(item.prezzo.ToString());
                            lvi.SubItems.Add(item.quantita.ToString());
                            lvi.Tag = item.ID;
                            lsvprodotti.Items.Add(lvi);
                        }
                    }
                    rimuoviquantitacarrello();
                }
            }
            if (btn.Name == "btn3")
            {
                ClsProdotto prodotto = new ClsProdotto();
                prodotti = prodotto.getProdotti();
                if (prodotti != null)
                {
                    lsvprodotti.Items.Clear();
                    foreach (var item in prodotti)
                    {
                        if (item.IDCategoria == 3)
                        {
                            ListViewItem lvi = new ListViewItem(item.nome);
                            lvi.SubItems.Add(item.prezzo.ToString());
                            lvi.SubItems.Add(item.quantita.ToString());

                            lsvprodotti.Items.Add(lvi);
                        }
                    }
                    rimuoviquantitacarrello();
                    //MessageBox.Show("Categoria " + btn.Text + " selezionata");
                }
            }
            if (btn.Name == "btn4")
            {
                ClsProdotto prodotto = new ClsProdotto();
                prodotti = prodotto.getProdotti();
                if (prodotti != null)
                {
                    lsvprodotti.Items.Clear();
                    foreach (var item in prodotti)
                    {
                        if (item.IDCategoria == 4)
                        {
                            ListViewItem lvi = new ListViewItem(item.nome);
                            lvi.SubItems.Add(item.prezzo.ToString());
                            lvi.SubItems.Add(item.quantita.ToString());

                            lsvprodotti.Items.Add(lvi);
                        }
                    }
                    rimuoviquantitacarrello();
                    //MessageBox.Show("Categoria " + btn.Text + " selezionata");
                }
            }
            if (btn.Name == "btn5")
            {
                ClsProdotto prodotto = new ClsProdotto();
                prodotti = prodotto.getProdotti();
                if (prodotti != null)
                {
                    lsvprodotti.Items.Clear();
                    foreach (var item in prodotti)
                    {
                        if (item.IDCategoria == 5)
                        {
                            ListViewItem lvi = new ListViewItem(item.nome);
                            lvi.SubItems.Add(item.prezzo.ToString());
                            lvi.SubItems.Add(item.quantita.ToString());

                            lsvprodotti.Items.Add(lvi);
                        }
                    }
                    rimuoviquantitacarrello();
                    //MessageBox.Show("Categoria " + btn.Text + " selezionata");
                }
            }
        }

        private void rimuoviquantitacarrello()
        {
            foreach (ListViewItem item in lsvprodotti.Items)
            {
                foreach (ListViewItem carrello in lsvCarrello.Items)
                {
                    if (item.SubItems[0].Text == carrello.SubItems[0].Text)
                    {
                        item.SubItems[2].Text = (Convert.ToInt32(item.SubItems[2].Text) - Convert.ToInt32(carrello.SubItems[2].Text)).ToString();
                    }
                }
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            loadProdotti();
        }

        private void pnlordine_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnProdotti_Click(object sender, EventArgs e)
        {
            FrmProdotti f = new FrmProdotti();
            this.Hide();
            f.Show();
        }

        private void btnOrdini_Click(object sender, EventArgs e)
        {
            FrmOrdini f = new FrmOrdini();
            this.Hide();
            f.Show();
        }

        private void btnBrioches_Click(object sender, EventArgs e)
        {
            loadProdotti();
        }

        private void btnPizza_Click(object sender, EventArgs e)
        {
            loadProdotti();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void btnFocaccia_Click(object sender, EventArgs e)
        {
            loadProdotti();
        }

        private void btnBevande_Click(object sender, EventArgs e)
        {
            loadProdotti();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void btnAggiungiOrdine_Click(object sender, EventArgs e)
        {

            int i = 0;
            int indice = -1;
            if (lsvprodotti.SelectedItems.Count == 1)
            {

                foreach (ListViewItem item in lsvprodotti.SelectedItems)
                {
                    if (udQuantita.Value <= Convert.ToInt32(item.SubItems[2].Text))
                    {
                        if (Convert.ToInt32(item.SubItems[2].Text) > 0)
                        {
                            foreach (ListViewItem carrello in lsvCarrello.Items)
                            {
                                if (carrello.SubItems[0].Text == item.SubItems[0].Text)
                                {
                                    indice = i;
                                }
                                i++;
                            }
                            if (indice != -1)
                            {
                                item.SubItems[2].Text = Convert.ToString(Convert.ToInt32(item.SubItems[2].Text) - udQuantita.Value);
                                lsvCarrello.Items[indice].SubItems[2].Text = (udQuantita.Value + Convert.ToInt32(lsvCarrello.Items[indice].SubItems[2].Text)).ToString();
                                prezzo = prezzo + Convert.ToDouble(lsvCarrello.Items[indice].SubItems[1].Text) * Convert.ToInt32(udQuantita.Value);
                            }
                            else
                            {
                                ListViewItem temp = (ListViewItem)item.Clone();
                                temp.SubItems[2].Text = udQuantita.Value.ToString();
                                lsvCarrello.Items.Add(temp);
                                //carrello.SubItems[2].Text = udQuantita.Value.ToString();
                                item.SubItems[2].Text = Convert.ToString(Convert.ToInt32(item.SubItems[2].Text) - udQuantita.Value);
                                prezzo = prezzo + Convert.ToDouble(temp.SubItems[1].Text) * Convert.ToInt32(udQuantita.Value);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Il prodotto selezionato è esaurito");
                        }
                    }
                    else
                    {
                        MessageBox.Show("La quantità selezionata è maggiore di quella disponibile");
                    }

                    // lsvCarrello.Items.Add(udQuantita.Value.ToString(), 1);
                }
            }
            else
            {
                MessageBox.Show("Devi selezionare una riga dalla list view.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            label10.Text = "Prezzo Ordine: " + prezzo + "€";
            btnEliminaCarrello.Enabled = true;
            btnConfermaOrdine.Enabled = true;
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void lsvprodotti_SelectedIndexChanged(object sender, EventArgs e)
        {
            aggiornamax();
        }

        private void FrmOrdine_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        #endregion

        private void btnordinaora_Click(object sender, EventArgs e)
        {

        }

        private void btnEliminaCarrello_Click(object sender, EventArgs e)
        {

            if (lsvCarrello.SelectedItems.Count == 1)
            {
                /*prodotto = new ClsProdotto();
                char ID = (char)lsvCarrello.SelectedItems[0].Tag;
                DialogResult dr = MessageBox.Show("Sei sicuro di eliminare questo prodotto:" + prodotto.Nome + " " + prodotto.Prezzo + " " + prodotto.Quantita, "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    string msg = prodotto.deleteProdotto();
                    MessageBox.Show(msg);
                    prodotto = new ClsProdotto();
                    //Aggiorna(prodotto);
                }*/
                foreach (ListViewItem selectedItem in lsvCarrello.SelectedItems)
                {
                    foreach (ListViewItem item in lsvprodotti.SelectedItems)
                    {
                        if (item.SubItems[0].Text == selectedItem.SubItems[0].Text)
                        {
                            item.SubItems[2].Text = (Convert.ToInt32(selectedItem.SubItems[2].Text) + Convert.ToInt32(item.SubItems[2].Text)).ToString();
                            prezzo = prezzo - Convert.ToDouble(selectedItem.SubItems[1].Text) * Convert.ToInt32(selectedItem.SubItems[2].Text);
                            lsvCarrello.Items.Remove(selectedItem);
                            label10.Text = "Prezzo Ordine: " + prezzo + "€";
                        }
                        else
                        {
                            MessageBox.Show("Gli elementi selezionati nelle 2 liste sono diversi");
                        }
                    }
                }

                //prodotti = prodotti.First(p => p. == ID);

            }
            else
            {
                MessageBox.Show("Devi selezionare un articolo nel carrello.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            aggiornamax();
            if (lsvCarrello.Items.Count == 0)
            {
                btnConfermaOrdine.Enabled = false;
                btnEliminaCarrello.Enabled = false;
            }

        }

        private void btnConfermaOrdine_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lsvCarrello.Items)
            {
                if (item.SubItems[2].Text == noQuantita.ToString())
                {
                    MessageBox.Show("Non è possibile ordinare uno snack con quantità 0", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            /*ClsOrdiniProdotti ordine = new ClsOrdiniProdotti();
            try
            {
                ordine.IDProdotto= Program.;
                ordine.
               
                    string msg = ordine.createOrdine();
               
               
                    MessageBox.Show(ordine.updateOrdine());
                    edit = false;                
            }
            catch (Exception)
            {
                MessageBox.Show("RICONTROLLA I CAMPI");
                throw;
            }*/
        }


        private void btnsalvaordini_Click(object sender, EventArgs e)
        {

        }

        private void udQuantita_ValueChanged(object sender, EventArgs e)
        {

            /*if (lsvCarrello.SelectedItems.Count == 1)
            {
                foreach (ListViewItem item in lsvCarrello.SelectedItems)

                {
                    item.SubItems[2].Text = udQuantita.Value.ToString();
                }
            }*/
        }
    }
}