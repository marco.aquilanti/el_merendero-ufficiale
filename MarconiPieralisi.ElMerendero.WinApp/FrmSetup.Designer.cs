﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSetup));
            this.label3 = new System.Windows.Forms.Label();
            this.tbvalore = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbchiave = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btneliminasetup = new System.Windows.Forms.Button();
            this.btnmodificasetup = new System.Windows.Forms.Button();
            this.btnaggiungisetup = new System.Windows.Forms.Button();
            this.btaggiorna = new System.Windows.Forms.Button();
            this.btnSetup = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lsvSetup = new System.Windows.Forms.ListView();
            this.Chiave = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Valore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.btnsalvasetup = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(550, 161);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 19);
            this.label3.TabIndex = 26;
            this.label3.Text = "Valore";
            // 
            // tbvalore
            // 
            this.tbvalore.BackColor = System.Drawing.SystemColors.Control;
            this.tbvalore.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbvalore.Location = new System.Drawing.Point(554, 182);
            this.tbvalore.Margin = new System.Windows.Forms.Padding(2);
            this.tbvalore.Name = "tbvalore";
            this.tbvalore.Size = new System.Drawing.Size(152, 25);
            this.tbvalore.TabIndex = 25;
            this.tbvalore.Text = "Valore";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(550, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 19);
            this.label1.TabIndex = 24;
            this.label1.Text = "Chiave";
            // 
            // tbchiave
            // 
            this.tbchiave.BackColor = System.Drawing.SystemColors.Control;
            this.tbchiave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbchiave.Location = new System.Drawing.Point(554, 108);
            this.tbchiave.Margin = new System.Windows.Forms.Padding(2);
            this.tbchiave.Name = "tbchiave";
            this.tbchiave.Size = new System.Drawing.Size(152, 25);
            this.tbchiave.TabIndex = 23;
            this.tbchiave.Text = "Chiave";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(486, 157);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 22;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(486, 87);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 21;
            this.pictureBox4.TabStop = false;
            // 
            // btneliminasetup
            // 
            this.btneliminasetup.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneliminasetup.Location = new System.Drawing.Point(323, 235);
            this.btneliminasetup.Name = "btneliminasetup";
            this.btneliminasetup.Size = new System.Drawing.Size(95, 33);
            this.btneliminasetup.TabIndex = 71;
            this.btneliminasetup.Text = "Elimina";
            this.btneliminasetup.UseVisualStyleBackColor = true;
            this.btneliminasetup.Click += new System.EventHandler(this.btneliminasetup_Click);
            // 
            // btnmodificasetup
            // 
            this.btnmodificasetup.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodificasetup.Location = new System.Drawing.Point(323, 196);
            this.btnmodificasetup.Name = "btnmodificasetup";
            this.btnmodificasetup.Size = new System.Drawing.Size(95, 33);
            this.btnmodificasetup.TabIndex = 70;
            this.btnmodificasetup.Text = "Modifica";
            this.btnmodificasetup.UseVisualStyleBackColor = true;
            this.btnmodificasetup.Click += new System.EventHandler(this.btnmodificasetup_Click);
            // 
            // btnaggiungisetup
            // 
            this.btnaggiungisetup.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiungisetup.Location = new System.Drawing.Point(323, 157);
            this.btnaggiungisetup.Name = "btnaggiungisetup";
            this.btnaggiungisetup.Size = new System.Drawing.Size(95, 33);
            this.btnaggiungisetup.TabIndex = 69;
            this.btnaggiungisetup.Text = "Aggiungi";
            this.btnaggiungisetup.UseVisualStyleBackColor = true;
            this.btnaggiungisetup.Click += new System.EventHandler(this.btnaggiungisetup_Click);
            // 
            // btaggiorna
            // 
            this.btaggiorna.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btaggiorna.Location = new System.Drawing.Point(323, 85);
            this.btaggiorna.Name = "btaggiorna";
            this.btaggiorna.Size = new System.Drawing.Size(95, 33);
            this.btaggiorna.TabIndex = 72;
            this.btaggiorna.Text = "Aggiorna";
            this.btaggiorna.UseVisualStyleBackColor = true;
            this.btaggiorna.Click += new System.EventHandler(this.btaggiorna_Click);
            // 
            // btnSetup
            // 
            this.btnSetup.BackColor = System.Drawing.Color.Transparent;
            this.btnSetup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSetup.BackgroundImage")));
            this.btnSetup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSetup.Location = new System.Drawing.Point(43, 15);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(50, 50);
            this.btnSetup.TabIndex = 149;
            this.btnSetup.Text = " ";
            this.btnSetup.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(119, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 19);
            this.label2.TabIndex = 148;
            this.label2.Text = "Setup";
            // 
            // lsvSetup
            // 
            this.lsvSetup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Chiave,
            this.Valore});
            this.lsvSetup.FullRowSelect = true;
            this.lsvSetup.HideSelection = false;
            this.lsvSetup.Location = new System.Drawing.Point(43, 85);
            this.lsvSetup.Name = "lsvSetup";
            this.lsvSetup.Size = new System.Drawing.Size(274, 330);
            this.lsvSetup.TabIndex = 147;
            this.lsvSetup.UseCompatibleStateImageBehavior = false;
            this.lsvSetup.View = System.Windows.Forms.View.Details;
            // 
            // Chiave
            // 
            this.Chiave.Text = "Chiave";
            this.Chiave.Width = 108;
            // 
            // Valore
            // 
            this.Valore.Text = "Valore";
            this.Valore.Width = 151;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(591, 301);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 19);
            this.label5.TabIndex = 151;
            this.label5.Text = "Salva";
            // 
            // btnsalvasetup
            // 
            this.btnsalvasetup.BackColor = System.Drawing.Color.Transparent;
            this.btnsalvasetup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalvasetup.BackgroundImage")));
            this.btnsalvasetup.Enabled = false;
            this.btnsalvasetup.Location = new System.Drawing.Point(554, 295);
            this.btnsalvasetup.Name = "btnsalvasetup";
            this.btnsalvasetup.Size = new System.Drawing.Size(32, 32);
            this.btnsalvasetup.TabIndex = 150;
            this.btnsalvasetup.Text = " ";
            this.btnsalvasetup.UseVisualStyleBackColor = false;
            this.btnsalvasetup.Click += new System.EventHandler(this.btnsalvasetup_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(551, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 47);
            this.label4.TabIndex = 152;
            this.label4.Text = "Se si inserisce un orario va usato obbligatoriamente il formato hh:mm";
            // 
            // FrmSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 462);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnsalvasetup);
            this.Controls.Add(this.btnSetup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lsvSetup);
            this.Controls.Add(this.btaggiorna);
            this.Controls.Add(this.btneliminasetup);
            this.Controls.Add(this.btnmodificasetup);
            this.Controls.Add(this.btnaggiungisetup);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbvalore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbchiave);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Name = "FrmSetup";
            this.Text = "FrmSetup";
            this.Load += new System.EventHandler(this.FrmSetup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbvalore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbchiave;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button btneliminasetup;
        private System.Windows.Forms.Button btnmodificasetup;
        private System.Windows.Forms.Button btnaggiungisetup;
        private System.Windows.Forms.Button btaggiorna;
        private System.Windows.Forms.Button btnSetup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lsvSetup;
        private System.Windows.Forms.ColumnHeader Chiave;
        private System.Windows.Forms.ColumnHeader Valore;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnsalvasetup;
        private System.Windows.Forms.Label label4;
    }
}