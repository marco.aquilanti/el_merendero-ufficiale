﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmPuntoVendita
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPuntoVendita));
            this.label5 = new System.Windows.Forms.Label();
            this.btnsalvautente = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnaggiorna = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnelimina = new System.Windows.Forms.Button();
            this.tbnomeprodotto = new System.Windows.Forms.TextBox();
            this.btnmodifica = new System.Windows.Forms.Button();
            this.tbdescrizione = new System.Windows.Forms.TextBox();
            this.btnaggiungi = new System.Windows.Forms.Button();
            this.lsvprodotti = new System.Windows.Forms.ListView();
            this.tbnome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbDescrizione1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnPuntoVendita = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(696, 165);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 19);
            this.label5.TabIndex = 104;
            this.label5.Text = "Salva";
            // 
            // btnsalvautente
            // 
<<<<<<< HEAD
            this.btnsalvautente.BackColor = System.Drawing.Color.Transparent;
            this.btnsalvautente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalvautente.BackgroundImage")));
            this.btnsalvautente.Location = new System.Drawing.Point(581, 380);
            this.btnsalvautente.Name = "btnsalvautente";
            this.btnsalvautente.Size = new System.Drawing.Size(32, 32);
            this.btnsalvautente.TabIndex = 103;
            this.btnsalvautente.Text = " ";
            this.btnsalvautente.UseVisualStyleBackColor = false;
=======
            this.btnsalvapuntovendita.BackColor = System.Drawing.Color.Transparent;
            this.btnsalvapuntovendita.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalvapuntovendita.BackgroundImage")));
            this.btnsalvapuntovendita.Location = new System.Drawing.Point(659, 152);
            this.btnsalvapuntovendita.Name = "btnsalvapuntovendita";
            this.btnsalvapuntovendita.Size = new System.Drawing.Size(32, 32);
            this.btnsalvapuntovendita.TabIndex = 103;
            this.btnsalvapuntovendita.Text = " ";
            this.btnsalvapuntovendita.UseVisualStyleBackColor = false;
            this.btnsalvapuntovendita.Click += new System.EventHandler(this.btnsalvapuntovendita_Click);
>>>>>>> master
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(518, 82);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 19);
            this.label2.TabIndex = 106;
            this.label2.Text = "Nome:";
            // 
            // btnaggiorna
            // 
            this.btnaggiorna.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiorna.Location = new System.Drawing.Point(353, 82);
            this.btnaggiorna.Name = "btnaggiorna";
            this.btnaggiorna.Size = new System.Drawing.Size(95, 33);
            this.btnaggiorna.TabIndex = 102;
            this.btnaggiorna.Text = "Aggiorna";
            this.btnaggiorna.UseVisualStyleBackColor = true;
            this.btnaggiorna.Click += new System.EventHandler(this.btnaggiorna_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(472, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 19);
            this.label3.TabIndex = 107;
            this.label3.Text = "Descrizione:";
            // 
            // btnelimina
            // 
            this.btnelimina.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelimina.Location = new System.Drawing.Point(353, 263);
            this.btnelimina.Name = "btnelimina";
            this.btnelimina.Size = new System.Drawing.Size(95, 33);
            this.btnelimina.TabIndex = 101;
            this.btnelimina.Text = "Elimina";
            this.btnelimina.UseVisualStyleBackColor = true;
            this.btnelimina.Click += new System.EventHandler(this.btnelimina_Click);
            // 
            // tbnomeprodotto
            // 
            this.tbnomeprodotto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
<<<<<<< HEAD
            this.tbnomeprodotto.Location = new System.Drawing.Point(581, 258);
=======
            this.tbnomeprodotto.Location = new System.Drawing.Point(593, 83);
            this.tbnomeprodotto.MaxLength = 1;
>>>>>>> master
            this.tbnomeprodotto.Name = "tbnomeprodotto";
            this.tbnomeprodotto.Size = new System.Drawing.Size(100, 22);
            this.tbnomeprodotto.TabIndex = 108;
            // 
            // btnmodifica
            // 
            this.btnmodifica.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodifica.Location = new System.Drawing.Point(353, 224);
            this.btnmodifica.Name = "btnmodifica";
            this.btnmodifica.Size = new System.Drawing.Size(95, 33);
            this.btnmodifica.TabIndex = 100;
            this.btnmodifica.Text = "Modifica";
            this.btnmodifica.UseVisualStyleBackColor = true;
            this.btnmodifica.Click += new System.EventHandler(this.btnmodifica_Click);
            // 
            // tbdescrizione
            // 
            this.tbdescrizione.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbdescrizione.Location = new System.Drawing.Point(593, 111);
            this.tbdescrizione.Name = "tbdescrizione";
            this.tbdescrizione.Size = new System.Drawing.Size(163, 22);
            this.tbdescrizione.TabIndex = 109;
            // 
            // btnaggiungi
            // 
            this.btnaggiungi.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiungi.Location = new System.Drawing.Point(353, 185);
            this.btnaggiungi.Name = "btnaggiungi";
            this.btnaggiungi.Size = new System.Drawing.Size(95, 33);
            this.btnaggiungi.TabIndex = 99;
            this.btnaggiungi.Text = "Aggiungi";
            this.btnaggiungi.UseVisualStyleBackColor = true;
            this.btnaggiungi.Click += new System.EventHandler(this.btnaggiungi_Click);
            // 
            // lsvprodotti
            // 
            this.lsvprodotti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tbnome,
            this.tbDescrizione1});
            this.lsvprodotti.FullRowSelect = true;
            this.lsvprodotti.HideSelection = false;
            this.lsvprodotti.Location = new System.Drawing.Point(51, 82);
            this.lsvprodotti.Name = "lsvprodotti";
            this.lsvprodotti.Size = new System.Drawing.Size(274, 330);
            this.lsvprodotti.TabIndex = 98;
            this.lsvprodotti.UseCompatibleStateImageBehavior = false;
            this.lsvprodotti.View = System.Windows.Forms.View.Details;
            // 
            // tbnome
            // 
            this.tbnome.Text = "Nome";
            // 
            // tbDescrizione1
            // 
            this.tbDescrizione1.Text = "Descrizione";
            this.tbDescrizione1.Width = 201;
            // 
            // btnPuntoVendita
            // 
            this.btnPuntoVendita.BackColor = System.Drawing.Color.Transparent;
            this.btnPuntoVendita.BackgroundImage = global::MarconiPieralisi.ElMerendero.WinApp.Properties.Resources.puntovendita;
            this.btnPuntoVendita.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPuntoVendita.Location = new System.Drawing.Point(51, 12);
            this.btnPuntoVendita.Name = "btnPuntoVendita";
            this.btnPuntoVendita.Size = new System.Drawing.Size(50, 50);
            this.btnPuntoVendita.TabIndex = 146;
            this.btnPuntoVendita.Text = " ";
            this.btnPuntoVendita.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(127, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 19);
            this.label1.TabIndex = 145;
            this.label1.Text = "Punto Vendita";
            // 
            // FrmPuntoVendita
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 434);
            this.Controls.Add(this.btnPuntoVendita);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lsvprodotti);
            this.Controls.Add(this.btnaggiungi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbdescrizione);
            this.Controls.Add(this.btnsalvautente);
            this.Controls.Add(this.btnmodifica);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbnomeprodotto);
            this.Controls.Add(this.btnaggiorna);
            this.Controls.Add(this.btnelimina);
            this.Controls.Add(this.label3);
            this.Name = "FrmPuntoVendita";
            this.Text = "FrmPuntoVendita";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPuntoVendita_FormClosing);
            this.Load += new System.EventHandler(this.FrmPuntoVendita_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnsalvautente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnaggiorna;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnelimina;
        private System.Windows.Forms.TextBox tbnomeprodotto;
        private System.Windows.Forms.Button btnmodifica;
        private System.Windows.Forms.TextBox tbdescrizione;
        private System.Windows.Forms.Button btnaggiungi;
        private System.Windows.Forms.ListView lsvprodotti;
        private System.Windows.Forms.ColumnHeader tbnome;
        private System.Windows.Forms.ColumnHeader tbDescrizione1;
        private System.Windows.Forms.Button btnPuntoVendita;
        private System.Windows.Forms.Label label1;
    }
}