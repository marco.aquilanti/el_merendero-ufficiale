﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Net;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmProdotti :Form
    {
        public FrmProdotti()
        {
            InitializeComponent();
            disattiva();
        }
        ClsProdotto prodotto = new ClsProdotto();
        bool edit=false;

        private void loadProdotti()
        {
            
            ClsProdotto prodotto = new ClsProdotto();
            List<ClsProdotto> prodotti = prodotto.getProdotti();

            lsvprodotti.Items.Clear();
            foreach (ClsProdotto item in prodotti)
            {
                ListViewItem lvi = new ListViewItem(item.Nome);
                lvi.SubItems.Add(Convert.ToString(item.Quantita));
                lvi.SubItems.Add(item.Descrizione);
                lvi.SubItems.Add(Convert.ToString(item.Prezzo));
                lsvprodotti.Items.Add(lvi);
            }
        }
        private void attiva()
        {
            txtnomeprodotto.Enabled = true;
            numQuantità.Enabled = true;
            txtdescrizione.Enabled = true;
            numPrezzo.Enabled = true;
            txtCategoria.Enabled = true;
        }
        private void disattiva()
        {
            txtnomeprodotto.Enabled = false;
            numQuantità.Enabled = false;
            txtdescrizione.Enabled = false;
            numPrezzo.Enabled = false;
            txtCategoria.Enabled = false;
        }
        
        private void btnaggiungi_Click(object sender, EventArgs e)
        {
            attiva();
            edit = false;
            loadProdotti();
        }

        private void btnmodifica_Click(object sender, EventArgs e)
        {
            attiva();
            edit = true;
            if (lsvprodotti.SelectedIndices.Count == 1)
            {
                int indice = lsvprodotti.SelectedIndices[0];
                if (indice>=0)
                {
                    ClsProdotto prodotto = new ClsProdotto();
                    prodotto.Nome = txtnomeprodotto.Text;
                    prodotto.Quantita = Convert.ToInt32(numQuantità.Text);
                    prodotto.Descrizione = txtdescrizione.Text;
                    prodotto.Prezzo = Convert.ToSingle(numPrezzo.Text);
                    prodotto.IdCategoria = Convert.ToInt32(txtCategoria.Text);
                }             
            }
        }

        private void btnelimina_Click(object sender, EventArgs e)
        {
            if (lsvprodotti.SelectedItems.Count == 1)
            {
                ClsProdotto prodotto = new ClsProdotto();
                prodotto.Nome = lsvprodotti.SelectedItems[0].Text;

                string msg = prodotto.deleteProdotto();
                MessageBox.Show(msg);
                loadProdotti();
            }
        }

        private void btnsalvautente_Click_1(object sender, EventArgs e)
        {
            //try
            //{
            //    prodotto.Nome = txtnomeprodotto.Text;
            //    prodotto.Quantità = Convert.ToInt32(numQuantità.Text);
            //    prodotto.Descrizione = txtdescrizione.Text;
            //    prodotto.Prezzo = Convert.ToSingle(numPrezzo.Text);
            //    prodotto.NomeCategoria = txtCategoria.Text;
            //    if (edit == false)
            //    {
            //        string msg = prodotto.createProdotto();
            //        loadProdotti();
            //    }
            //    else
            //    {
            //        string msg = prodotto.updateProdotto();
            //        loadProdotti();
            //        edit = false;
            //    }
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("RICONTROLLA I CAMPI");
            //    throw;
            //}
            //disattiva();
            try
            {
                prodotto.Nome = txtnomeprodotto.Text;
                prodotto.Quantita = Convert.ToInt32(numQuantità.Text);
                prodotto.Descrizione = txtdescrizione.Text;
                prodotto.Prezzo = Convert.ToSingle(numPrezzo.Text);
                prodotto.IdCategoria = Convert.ToInt32(txtCategoria.Text);
                if (edit == false)
                {
                    string msg = prodotto.createProdotto();
                }
                else
                {
                    MessageBox.Show(prodotto.updateProdotto());
                    edit = false;
                }
                loadProdotti();
                disattiva();
            }
            catch (Exception)
            {
                MessageBox.Show("RICONTROLLA I CAMPI");
                throw;
            }
        }

        //private void lsvprodotti_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (lsvprodotti.SelectedItems.Count == 1)
        //    {
        //        txtnomeprodotto.Text = lsvprodotti.SelectedItems[0].Text;
        //        //txtquantità.Text = lsvprodotti.SelectedItems[0].SubItems[1].Text;
        //        txtdescrizione.Text = lsvprodotti.SelectedItems[0].SubItems[2].Text;
        //        //txtprezzo.Text = lsvprodotti.SelectedItems[0].SubItems[2].Text;
        //    }
        //}

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            FrmUtentiAdm f = new FrmUtentiAdm();
            this.Hide();
            f.Show();
        }

        private void btnClassi_Click(object sender, EventArgs e)
        {

            FrmClassi f = new FrmClassi();
            this.Hide();
            f.Show();
        }

        private void btnalunni_Click(object sender, EventArgs e)
        {

            FrmUtentiAdm f = new FrmUtentiAdm();
            this.Hide();
            f.Show();
        }

        private void btnaggiorna_Click(object sender, EventArgs e)
        {
            loadProdotti();
        }

        private void FrmProdotti_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void FrmProdotti_Load(object sender, EventArgs e)
        {

        }
    }
}
