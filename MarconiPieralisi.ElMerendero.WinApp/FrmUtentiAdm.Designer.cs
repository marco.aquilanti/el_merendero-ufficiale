﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmUtentiAdm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUtentiAdm));
            this.rbMerendero = new System.Windows.Forms.RadioButton();
            this.rbPersonaleScolastico = new System.Windows.Forms.RadioButton();
            this.rbAmministratore = new System.Windows.Forms.RadioButton();
            this.rbStudente = new System.Windows.Forms.RadioButton();
            this.btAggiorna = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbClassiFIltro = new System.Windows.Forms.ComboBox();
            this.cbTipologiaFiltro = new System.Windows.Forms.ComboBox();
            this.lsvalunni = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnimportaalunni = new System.Windows.Forms.Button();
            this.btnelimina = new System.Windows.Forms.Button();
            this.btnmodifica = new System.Windows.Forms.Button();
            this.btnaggiungi = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbclasse = new System.Windows.Forms.ComboBox();
            this.tbemail = new System.Windows.Forms.TextBox();
            this.tbcognome = new System.Windows.Forms.TextBox();
            this.tbnome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btClearClasse = new System.Windows.Forms.Button();
            this.btClearTipologia = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnsalvautente = new System.Windows.Forms.Button();
            this.clearTbNome = new System.Windows.Forms.Button();
            this.clearTbCognome = new System.Windows.Forms.Button();
            this.clearTbEmail = new System.Windows.Forms.Button();
            this.clearChecked = new System.Windows.Forms.Button();
            this.btClearClasseDx = new System.Windows.Forms.Button();
            this.cbMerendino = new System.Windows.Forms.CheckBox();
            this.cbCandidabile = new System.Windows.Forms.CheckBox();
            this.cbBloccato = new System.Windows.Forms.CheckBox();
            this.lblNumUtenti = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // rbMerendero
            // 
            this.rbMerendero.AutoSize = true;
            this.rbMerendero.Enabled = false;
            this.rbMerendero.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMerendero.Location = new System.Drawing.Point(885, 357);
            this.rbMerendero.Name = "rbMerendero";
            this.rbMerendero.Size = new System.Drawing.Size(113, 24);
            this.rbMerendero.TabIndex = 104;
            this.rbMerendero.TabStop = true;
            this.rbMerendero.Text = "Merendero";
            this.rbMerendero.UseVisualStyleBackColor = true;
            this.rbMerendero.CheckedChanged += new System.EventHandler(this.rbMerendero_CheckedChanged);
            // 
            // rbPersonaleScolastico
            // 
            this.rbPersonaleScolastico.AutoSize = true;
            this.rbPersonaleScolastico.Enabled = false;
            this.rbPersonaleScolastico.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbPersonaleScolastico.Location = new System.Drawing.Point(885, 334);
            this.rbPersonaleScolastico.Name = "rbPersonaleScolastico";
            this.rbPersonaleScolastico.Size = new System.Drawing.Size(192, 24);
            this.rbPersonaleScolastico.TabIndex = 103;
            this.rbPersonaleScolastico.TabStop = true;
            this.rbPersonaleScolastico.Text = "Personale scolastico";
            this.rbPersonaleScolastico.UseVisualStyleBackColor = true;
            this.rbPersonaleScolastico.CheckedChanged += new System.EventHandler(this.rbPersonaleScolastico_CheckedChanged);
            // 
            // rbAmministratore
            // 
            this.rbAmministratore.AutoSize = true;
            this.rbAmministratore.Enabled = false;
            this.rbAmministratore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAmministratore.Location = new System.Drawing.Point(885, 311);
            this.rbAmministratore.Name = "rbAmministratore";
            this.rbAmministratore.Size = new System.Drawing.Size(148, 24);
            this.rbAmministratore.TabIndex = 102;
            this.rbAmministratore.TabStop = true;
            this.rbAmministratore.Text = "Amministratore";
            this.rbAmministratore.UseVisualStyleBackColor = true;
            this.rbAmministratore.CheckedChanged += new System.EventHandler(this.rbAmministratore_CheckedChanged);
            // 
            // rbStudente
            // 
            this.rbStudente.AutoSize = true;
            this.rbStudente.Enabled = false;
            this.rbStudente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbStudente.Location = new System.Drawing.Point(885, 288);
            this.rbStudente.Name = "rbStudente";
            this.rbStudente.Size = new System.Drawing.Size(101, 24);
            this.rbStudente.TabIndex = 101;
            this.rbStudente.TabStop = true;
            this.rbStudente.Text = "Studente";
            this.rbStudente.UseVisualStyleBackColor = true;
            this.rbStudente.CheckedChanged += new System.EventHandler(this.rbStudente_CheckedChanged);
            // 
            // btAggiorna
            // 
            this.btAggiorna.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAggiorna.Location = new System.Drawing.Point(628, 120);
            this.btAggiorna.Name = "btAggiorna";
            this.btAggiorna.Size = new System.Drawing.Size(72, 33);
            this.btAggiorna.TabIndex = 100;
            this.btAggiorna.Text = "Aggiorna";
            this.btAggiorna.UseVisualStyleBackColor = true;
            this.btAggiorna.Click += new System.EventHandler(this.btAggiorna_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(139, 104);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 19);
            this.label8.TabIndex = 99;
            this.label8.Text = "Tipologia";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(248, 105);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 19);
            this.label6.TabIndex = 98;
            this.label6.Text = "Classe";
            // 
            // cbClassiFIltro
            // 
            this.cbClassiFIltro.FormattingEnabled = true;
            this.cbClassiFIltro.Location = new System.Drawing.Point(252, 127);
            this.cbClassiFIltro.Name = "cbClassiFIltro";
            this.cbClassiFIltro.Size = new System.Drawing.Size(57, 21);
            this.cbClassiFIltro.TabIndex = 97;
            // 
            // cbTipologiaFiltro
            // 
            this.cbTipologiaFiltro.FormattingEnabled = true;
            this.cbTipologiaFiltro.Items.AddRange(new object[] {
            "Amministratore",
            "Studente",
            "Personale",
            "Merendero"});
            this.cbTipologiaFiltro.Location = new System.Drawing.Point(143, 126);
            this.cbTipologiaFiltro.Name = "cbTipologiaFiltro";
            this.cbTipologiaFiltro.Size = new System.Drawing.Size(76, 21);
            this.cbTipologiaFiltro.TabIndex = 96;
            // 
            // lsvalunni
            // 
            this.lsvalunni.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lsvalunni.FullRowSelect = true;
            this.lsvalunni.HideSelection = false;
            this.lsvalunni.Location = new System.Drawing.Point(47, 159);
            this.lsvalunni.Name = "lsvalunni";
            this.lsvalunni.Size = new System.Drawing.Size(653, 436);
            this.lsvalunni.TabIndex = 89;
            this.lsvalunni.UseCompatibleStateImageBehavior = false;
            this.lsvalunni.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nome";
            this.columnHeader1.Width = 98;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Cognome";
            this.columnHeader2.Width = 95;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Classe";
            this.columnHeader3.Width = 50;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Tipologia ";
            this.columnHeader4.Width = 59;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Email";
            this.columnHeader5.Width = 155;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Merendino";
            this.columnHeader6.Width = 64;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Candidabile";
            this.columnHeader7.Width = 67;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Bloccato";
            this.columnHeader8.Width = 55;
            // 
            // btnimportaalunni
            // 
            this.btnimportaalunni.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnimportaalunni.Location = new System.Drawing.Point(716, 301);
            this.btnimportaalunni.Name = "btnimportaalunni";
            this.btnimportaalunni.Size = new System.Drawing.Size(95, 33);
            this.btnimportaalunni.TabIndex = 95;
            this.btnimportaalunni.Text = "Importa";
            this.btnimportaalunni.UseVisualStyleBackColor = true;
            this.btnimportaalunni.Click += new System.EventHandler(this.btnimportaalunni_Click);
            // 
            // btnelimina
            // 
            this.btnelimina.Enabled = false;
            this.btnelimina.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelimina.Location = new System.Drawing.Point(716, 241);
            this.btnelimina.Name = "btnelimina";
            this.btnelimina.Size = new System.Drawing.Size(95, 33);
            this.btnelimina.TabIndex = 94;
            this.btnelimina.Text = "Elimina";
            this.btnelimina.UseVisualStyleBackColor = true;
            this.btnelimina.Click += new System.EventHandler(this.btnelimina_Click);
            // 
            // btnmodifica
            // 
            this.btnmodifica.Enabled = false;
            this.btnmodifica.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodifica.Location = new System.Drawing.Point(716, 180);
            this.btnmodifica.Name = "btnmodifica";
            this.btnmodifica.Size = new System.Drawing.Size(95, 33);
            this.btnmodifica.TabIndex = 93;
            this.btnmodifica.Text = "Modifica";
            this.btnmodifica.UseVisualStyleBackColor = true;
            this.btnmodifica.Click += new System.EventHandler(this.btnmodifica_Click);
            // 
            // btnaggiungi
            // 
            this.btnaggiungi.Enabled = false;
            this.btnaggiungi.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiungi.Location = new System.Drawing.Point(716, 119);
            this.btnaggiungi.Name = "btnaggiungi";
            this.btnaggiungi.Size = new System.Drawing.Size(95, 33);
            this.btnaggiungi.TabIndex = 92;
            this.btnaggiungi.Text = "Aggiungi";
            this.btnaggiungi.UseVisualStyleBackColor = true;
            this.btnaggiungi.Click += new System.EventHandler(this.btnaggiungi_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(862, 238);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 19);
            this.label4.TabIndex = 91;
            this.label4.Text = "Classe:";
            // 
            // cbclasse
            // 
            this.cbclasse.BackColor = System.Drawing.Color.White;
            this.cbclasse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbclasse.Enabled = false;
            this.cbclasse.FormattingEnabled = true;
            this.cbclasse.Items.AddRange(new object[] {
            "5AM",
            "5BM",
            "5CM"});
            this.cbclasse.Location = new System.Drawing.Point(939, 238);
            this.cbclasse.Name = "cbclasse";
            this.cbclasse.Size = new System.Drawing.Size(100, 21);
            this.cbclasse.TabIndex = 90;
            // 
            // tbemail
            // 
            this.tbemail.Enabled = false;
            this.tbemail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbemail.Location = new System.Drawing.Point(939, 201);
            this.tbemail.Name = "tbemail";
            this.tbemail.Size = new System.Drawing.Size(204, 22);
            this.tbemail.TabIndex = 86;
            // 
            // tbcognome
            // 
            this.tbcognome.Enabled = false;
            this.tbcognome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcognome.Location = new System.Drawing.Point(939, 161);
            this.tbcognome.Name = "tbcognome";
            this.tbcognome.Size = new System.Drawing.Size(204, 22);
            this.tbcognome.TabIndex = 85;
            // 
            // tbnome
            // 
            this.tbnome.Enabled = false;
            this.tbnome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbnome.Location = new System.Drawing.Point(939, 123);
            this.tbnome.Name = "tbnome";
            this.tbnome.Size = new System.Drawing.Size(204, 22);
            this.tbnome.TabIndex = 84;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(872, 201);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 19);
            this.label3.TabIndex = 83;
            this.label3.Text = "Email:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(872, 125);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 19);
            this.label2.TabIndex = 82;
            this.label2.Text = "Nome:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(842, 165);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 19);
            this.label1.TabIndex = 81;
            this.label1.Text = "Cognome:";
            // 
            // btClearClasse
            // 
            this.btClearClasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClearClasse.Location = new System.Drawing.Point(315, 127);
            this.btClearClasse.Name = "btClearClasse";
            this.btClearClasse.Size = new System.Drawing.Size(13, 21);
            this.btClearClasse.TabIndex = 112;
            this.btClearClasse.Text = "X";
            this.btClearClasse.UseVisualStyleBackColor = true;
            this.btClearClasse.Click += new System.EventHandler(this.btClearClasse_Click);
            // 
            // btClearTipologia
            // 
            this.btClearTipologia.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btClearTipologia.BackgroundImage")));
            this.btClearTipologia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClearTipologia.Location = new System.Drawing.Point(225, 126);
            this.btClearTipologia.Name = "btClearTipologia";
            this.btClearTipologia.Size = new System.Drawing.Size(13, 21);
            this.btClearTipologia.TabIndex = 113;
            this.btClearTipologia.Text = "X";
            this.btClearTipologia.UseVisualStyleBackColor = true;
            this.btClearTipologia.Click += new System.EventHandler(this.btClearTipologia_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(47, 30);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 114;
            this.pictureBox2.TabStop = false;
            // 
            // btnsalvautente
            // 
            this.btnsalvautente.BackColor = System.Drawing.Color.Transparent;
            this.btnsalvautente.Enabled = false;
            this.btnsalvautente.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnsalvautente.Location = new System.Drawing.Point(1067, 562);
            this.btnsalvautente.Name = "btnsalvautente";
            this.btnsalvautente.Size = new System.Drawing.Size(95, 33);
            this.btnsalvautente.TabIndex = 95;
            this.btnsalvautente.Text = " Salva";
            this.btnsalvautente.UseVisualStyleBackColor = false;
            this.btnsalvautente.Click += new System.EventHandler(this.btnsalvautente_Click);
            // 
            // clearTbNome
            // 
            this.clearTbNome.Enabled = false;
            this.clearTbNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearTbNome.Location = new System.Drawing.Point(1149, 123);
            this.clearTbNome.Name = "clearTbNome";
            this.clearTbNome.Size = new System.Drawing.Size(13, 22);
            this.clearTbNome.TabIndex = 115;
            this.clearTbNome.Text = "X";
            this.clearTbNome.UseVisualStyleBackColor = true;
            this.clearTbNome.Click += new System.EventHandler(this.clearTbNome_Click);
            // 
            // clearTbCognome
            // 
            this.clearTbCognome.Enabled = false;
            this.clearTbCognome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearTbCognome.Location = new System.Drawing.Point(1149, 161);
            this.clearTbCognome.Name = "clearTbCognome";
            this.clearTbCognome.Size = new System.Drawing.Size(13, 23);
            this.clearTbCognome.TabIndex = 116;
            this.clearTbCognome.Text = "X";
            this.clearTbCognome.UseVisualStyleBackColor = true;
            this.clearTbCognome.Click += new System.EventHandler(this.clearTbCognome_Click);
            // 
            // clearTbEmail
            // 
            this.clearTbEmail.Enabled = false;
            this.clearTbEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearTbEmail.Location = new System.Drawing.Point(1149, 201);
            this.clearTbEmail.Name = "clearTbEmail";
            this.clearTbEmail.Size = new System.Drawing.Size(13, 22);
            this.clearTbEmail.TabIndex = 117;
            this.clearTbEmail.Text = "X";
            this.clearTbEmail.UseVisualStyleBackColor = true;
            this.clearTbEmail.Click += new System.EventHandler(this.clearTbEmail_Click);
            // 
            // clearChecked
            // 
            this.clearChecked.Enabled = false;
            this.clearChecked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearChecked.Location = new System.Drawing.Point(850, 323);
            this.clearChecked.Name = "clearChecked";
            this.clearChecked.Size = new System.Drawing.Size(13, 21);
            this.clearChecked.TabIndex = 118;
            this.clearChecked.Text = "X";
            this.clearChecked.UseVisualStyleBackColor = true;
            this.clearChecked.Click += new System.EventHandler(this.clearChecked_Click);
            // 
            // btClearClasseDx
            // 
            this.btClearClasseDx.Enabled = false;
            this.btClearClasseDx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClearClasseDx.Location = new System.Drawing.Point(1064, 238);
            this.btClearClasseDx.Name = "btClearClasseDx";
            this.btClearClasseDx.Size = new System.Drawing.Size(13, 21);
            this.btClearClasseDx.TabIndex = 119;
            this.btClearClasseDx.Text = "X";
            this.btClearClasseDx.UseVisualStyleBackColor = true;
            this.btClearClasseDx.Click += new System.EventHandler(this.btClearClasseDx_Click);
            // 
            // cbMerendino
            // 
            this.cbMerendino.AutoSize = true;
            this.cbMerendino.Enabled = false;
            this.cbMerendino.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.cbMerendino.Location = new System.Drawing.Point(885, 420);
            this.cbMerendino.Name = "cbMerendino";
            this.cbMerendino.Size = new System.Drawing.Size(112, 24);
            this.cbMerendino.TabIndex = 120;
            this.cbMerendino.Text = "Merendino";
            this.cbMerendino.UseVisualStyleBackColor = true;
            // 
            // cbCandidabile
            // 
            this.cbCandidabile.AutoSize = true;
            this.cbCandidabile.Enabled = false;
            this.cbCandidabile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.cbCandidabile.Location = new System.Drawing.Point(885, 445);
            this.cbCandidabile.Name = "cbCandidabile";
            this.cbCandidabile.Size = new System.Drawing.Size(122, 24);
            this.cbCandidabile.TabIndex = 120;
            this.cbCandidabile.Text = "Candidabile";
            this.cbCandidabile.UseVisualStyleBackColor = true;
            this.cbCandidabile.CheckedChanged += new System.EventHandler(this.cbCandidabile_CheckedChanged);
            // 
            // cbBloccato
            // 
            this.cbBloccato.AutoSize = true;
            this.cbBloccato.Enabled = false;
            this.cbBloccato.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.cbBloccato.Location = new System.Drawing.Point(885, 470);
            this.cbBloccato.Name = "cbBloccato";
            this.cbBloccato.Size = new System.Drawing.Size(98, 24);
            this.cbBloccato.TabIndex = 120;
            this.cbBloccato.Text = "Bloccato";
            this.cbBloccato.UseVisualStyleBackColor = true;
            this.cbBloccato.CheckedChanged += new System.EventHandler(this.cbBloccato_CheckedChanged);
            // 
            // lblNumUtenti
            // 
            this.lblNumUtenti.AutoSize = true;
            this.lblNumUtenti.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumUtenti.Location = new System.Drawing.Point(43, 600);
            this.lblNumUtenti.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumUtenti.Name = "lblNumUtenti";
            this.lblNumUtenti.Size = new System.Drawing.Size(108, 18);
            this.lblNumUtenti.TabIndex = 121;
            this.lblNumUtenti.Text = "Numero utenti:";
            // 
            // FrmUtentiAdm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1212, 628);
            this.Controls.Add(this.lblNumUtenti);
            this.Controls.Add(this.cbBloccato);
            this.Controls.Add(this.cbCandidabile);
            this.Controls.Add(this.cbMerendino);
            this.Controls.Add(this.btClearClasseDx);
            this.Controls.Add(this.clearChecked);
            this.Controls.Add(this.clearTbEmail);
            this.Controls.Add(this.clearTbCognome);
            this.Controls.Add(this.clearTbNome);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btClearTipologia);
            this.Controls.Add(this.btClearClasse);
            this.Controls.Add(this.rbMerendero);
            this.Controls.Add(this.rbPersonaleScolastico);
            this.Controls.Add(this.rbAmministratore);
            this.Controls.Add(this.rbStudente);
            this.Controls.Add(this.btAggiorna);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbClassiFIltro);
            this.Controls.Add(this.cbTipologiaFiltro);
            this.Controls.Add(this.lsvalunni);
            this.Controls.Add(this.btnimportaalunni);
            this.Controls.Add(this.btnelimina);
            this.Controls.Add(this.btnmodifica);
            this.Controls.Add(this.btnaggiungi);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbclasse);
            this.Controls.Add(this.btnsalvautente);
            this.Controls.Add(this.tbemail);
            this.Controls.Add(this.tbcognome);
            this.Controls.Add(this.tbnome);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmUtentiAdm";
            this.Text = "FrmUtentiAdm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmUtentiAdm_FormClosing);
            this.Load += new System.EventHandler(this.FrmUtentiAdm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbMerendero;
        private System.Windows.Forms.RadioButton rbPersonaleScolastico;
        private System.Windows.Forms.RadioButton rbAmministratore;
        private System.Windows.Forms.RadioButton rbStudente;
        private System.Windows.Forms.Button btAggiorna;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbClassiFIltro;
        private System.Windows.Forms.ComboBox cbTipologiaFiltro;
        private System.Windows.Forms.ListView lsvalunni;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnimportaalunni;
        private System.Windows.Forms.Button btnelimina;
        private System.Windows.Forms.Button btnmodifica;
        private System.Windows.Forms.Button btnaggiungi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbclasse;
        private System.Windows.Forms.Button btnsalvautente;
        private System.Windows.Forms.TextBox tbemail;
        private System.Windows.Forms.TextBox tbcognome;
        private System.Windows.Forms.TextBox tbnome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btClearClasse;
        private System.Windows.Forms.Button btClearTipologia;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button clearTbNome;
        private System.Windows.Forms.Button clearTbCognome;
        private System.Windows.Forms.Button clearTbEmail;
        private System.Windows.Forms.Button clearChecked;
        private System.Windows.Forms.Button btClearClasseDx;
        private System.Windows.Forms.CheckBox cbMerendino;
        private System.Windows.Forms.CheckBox cbCandidabile;
        private System.Windows.Forms.CheckBox cbBloccato;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label lblNumUtenti;
    }
}