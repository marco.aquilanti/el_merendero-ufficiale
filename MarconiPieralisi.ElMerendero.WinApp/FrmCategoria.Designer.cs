﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmCategoria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCategoria));
            this.label1 = new System.Windows.Forms.Label();
            this.txtCategoria = new System.Windows.Forms.TextBox();
            this.txtnomeprodotto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnaggiorna = new System.Windows.Forms.Button();
            this.lsvprodotti = new System.Windows.Forms.ListView();
            this.tbID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbNomeCategoria = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbImgPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnelimina = new System.Windows.Forms.Button();
            this.btnmodifica = new System.Windows.Forms.Button();
            this.btnaggiungi = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnsalvautente = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(439, 115);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 19);
            this.label1.TabIndex = 114;
            this.label1.Text = "Immagine Path:";
            // 
            // txtCategoria
            // 
            this.txtCategoria.Location = new System.Drawing.Point(578, 114);
            this.txtCategoria.Name = "txtCategoria";
            this.txtCategoria.Size = new System.Drawing.Size(171, 20);
            this.txtCategoria.TabIndex = 113;
            // 
            // txtnomeprodotto
            // 
            this.txtnomeprodotto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnomeprodotto.Location = new System.Drawing.Point(578, 78);
            this.txtnomeprodotto.Name = "txtnomeprodotto";
            this.txtnomeprodotto.Size = new System.Drawing.Size(171, 22);
            this.txtnomeprodotto.TabIndex = 108;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(507, 81);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 19);
            this.label2.TabIndex = 106;
            this.label2.Text = "Nome:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(687, 167);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 19);
            this.label5.TabIndex = 104;
            this.label5.Text = "Salva";
            // 
            // btnaggiorna
            // 
            this.btnaggiorna.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiorna.Location = new System.Drawing.Point(340, 81);
            this.btnaggiorna.Name = "btnaggiorna";
            this.btnaggiorna.Size = new System.Drawing.Size(95, 33);
            this.btnaggiorna.TabIndex = 102;
            this.btnaggiorna.Text = "Aggiorna";
            this.btnaggiorna.UseVisualStyleBackColor = true;
            this.btnaggiorna.Click += new System.EventHandler(this.btnaggiorna_Click);
            // 
            // lsvprodotti
            // 
            this.lsvprodotti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tbID,
            this.tbNomeCategoria,
            this.tbImgPath});
            this.lsvprodotti.FullRowSelect = true;
            this.lsvprodotti.HideSelection = false;
            this.lsvprodotti.Location = new System.Drawing.Point(12, 78);
            this.lsvprodotti.Name = "lsvprodotti";
            this.lsvprodotti.Size = new System.Drawing.Size(322, 302);
            this.lsvprodotti.TabIndex = 98;
            this.lsvprodotti.UseCompatibleStateImageBehavior = false;
            this.lsvprodotti.View = System.Windows.Forms.View.Details;
            // 
            // tbID
            // 
            this.tbID.Text = "ID";
            this.tbID.Width = 42;
            // 
            // tbNomeCategoria
            // 
            this.tbNomeCategoria.Text = "Nome";
            this.tbNomeCategoria.Width = 110;
            // 
            // tbImgPath
            // 
            this.tbImgPath.Text = "Path img";
            this.tbImgPath.Width = 161;
            // 
            // btnelimina
            // 
            this.btnelimina.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelimina.Location = new System.Drawing.Point(340, 239);
            this.btnelimina.Name = "btnelimina";
            this.btnelimina.Size = new System.Drawing.Size(95, 33);
            this.btnelimina.TabIndex = 101;
            this.btnelimina.Text = "Elimina";
            this.btnelimina.UseVisualStyleBackColor = true;
            this.btnelimina.Click += new System.EventHandler(this.btnelimina_Click);
            // 
            // btnmodifica
            // 
            this.btnmodifica.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodifica.Location = new System.Drawing.Point(340, 200);
            this.btnmodifica.Name = "btnmodifica";
            this.btnmodifica.Size = new System.Drawing.Size(95, 33);
            this.btnmodifica.TabIndex = 100;
            this.btnmodifica.Text = "Modifica";
            this.btnmodifica.UseVisualStyleBackColor = true;
            this.btnmodifica.Click += new System.EventHandler(this.btnmodifica_Click);
            // 
            // btnaggiungi
            // 
            this.btnaggiungi.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiungi.Location = new System.Drawing.Point(340, 161);
            this.btnaggiungi.Name = "btnaggiungi";
            this.btnaggiungi.Size = new System.Drawing.Size(95, 33);
            this.btnaggiungi.TabIndex = 99;
            this.btnaggiungi.Text = "Aggiungi";
            this.btnaggiungi.UseVisualStyleBackColor = true;
            this.btnaggiungi.Click += new System.EventHandler(this.btnaggiungi_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::MarconiPieralisi.ElMerendero.WinApp.Properties.Resources.categoria_nero;
            this.pictureBox4.Location = new System.Drawing.Point(12, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(143, 60);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 115;
            this.pictureBox4.TabStop = false;
            // 
            // btnsalvautente
            // 
            this.btnsalvautente.BackColor = System.Drawing.Color.Transparent;
            this.btnsalvautente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalvautente.BackgroundImage")));
            this.btnsalvautente.Location = new System.Drawing.Point(650, 154);
            this.btnsalvautente.Name = "btnsalvautente";
            this.btnsalvautente.Size = new System.Drawing.Size(32, 32);
            this.btnsalvautente.TabIndex = 103;
            this.btnsalvautente.Text = " ";
            this.btnsalvautente.UseVisualStyleBackColor = false;
            this.btnsalvautente.Click += new System.EventHandler(this.btnsalvautente_Click);
            // 
            // FrmCategoria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 392);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCategoria);
            this.Controls.Add(this.txtnomeprodotto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnsalvautente);
            this.Controls.Add(this.btnaggiorna);
            this.Controls.Add(this.lsvprodotti);
            this.Controls.Add(this.btnelimina);
            this.Controls.Add(this.btnmodifica);
            this.Controls.Add(this.btnaggiungi);
            this.Name = "FrmCategoria";
            this.Text = "FrmCategorie";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCategoria_FormClosing);
            this.Load += new System.EventHandler(this.FrmCategoria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCategoria;
        private System.Windows.Forms.TextBox txtnomeprodotto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnsalvautente;
        private System.Windows.Forms.Button btnaggiorna;
        private System.Windows.Forms.ListView lsvprodotti;
        private System.Windows.Forms.ColumnHeader tbID;
        private System.Windows.Forms.Button btnelimina;
        private System.Windows.Forms.Button btnmodifica;
        private System.Windows.Forms.Button btnaggiungi;
        private System.Windows.Forms.ColumnHeader tbNomeCategoria;
        private System.Windows.Forms.ColumnHeader tbImgPath;
    }
}