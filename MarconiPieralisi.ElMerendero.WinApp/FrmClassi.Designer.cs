﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmClassi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmClassi));
            this.lsvClassi = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbistituto = new System.Windows.Forms.ComboBox();
            this.tbsezione = new System.Windows.Forms.TextBox();
            this.btnelimina = new System.Windows.Forms.Button();
            this.btnmodificaclasse = new System.Windows.Forms.Button();
            this.btnaggiungiclasse = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.cmbindirizzo = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.pnlclassi = new System.Windows.Forms.Panel();
            this.tbsigla = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnaggiorna = new System.Windows.Forms.Button();
            this.btnsalvaaula = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbplessoaula = new System.Windows.Forms.ComboBox();
            this.nupanno = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.tbaula = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnalunni = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pnlclassi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupanno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // lsvClassi
            // 
            this.lsvClassi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader7,
            this.columnHeader6,
            this.columnHeader10});
            this.lsvClassi.FullRowSelect = true;
            this.lsvClassi.HideSelection = false;
            this.lsvClassi.Location = new System.Drawing.Point(20, 3);
            this.lsvClassi.Name = "lsvClassi";
            this.lsvClassi.Size = new System.Drawing.Size(354, 387);
            this.lsvClassi.TabIndex = 65;
            this.lsvClassi.UseCompatibleStateImageBehavior = false;
            this.lsvClassi.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Sigla";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Anno";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Sezione";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Indirizzo";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "N. aula";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Plesso";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(699, 284);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 19);
            this.label17.TabIndex = 55;
            this.label17.Text = "Salva";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(563, 229);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 19);
            this.label16.TabIndex = 53;
            this.label16.Text = "Istituto:";
            // 
            // cmbistituto
            // 
            this.cmbistituto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbistituto.FormattingEnabled = true;
            this.cmbistituto.Items.AddRange(new object[] {
            "M",
            "P"});
            this.cmbistituto.Location = new System.Drawing.Point(647, 229);
            this.cmbistituto.Name = "cmbistituto";
            this.cmbistituto.Size = new System.Drawing.Size(106, 21);
            this.cmbistituto.TabIndex = 52;
            // 
            // tbsezione
            // 
            this.tbsezione.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbsezione.Location = new System.Drawing.Point(647, 87);
            this.tbsezione.Name = "tbsezione";
            this.tbsezione.Size = new System.Drawing.Size(106, 22);
            this.tbsezione.TabIndex = 51;
            // 
            // btnelimina
            // 
            this.btnelimina.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelimina.Location = new System.Drawing.Point(389, 168);
            this.btnelimina.Name = "btnelimina";
            this.btnelimina.Size = new System.Drawing.Size(95, 33);
            this.btnelimina.TabIndex = 68;
            this.btnelimina.Text = "Elimina";
            this.btnelimina.UseVisualStyleBackColor = true;
            this.btnelimina.Click += new System.EventHandler(this.btnelimina_Click);
            // 
            // btnmodificaclasse
            // 
            this.btnmodificaclasse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodificaclasse.Location = new System.Drawing.Point(389, 129);
            this.btnmodificaclasse.Name = "btnmodificaclasse";
            this.btnmodificaclasse.Size = new System.Drawing.Size(95, 33);
            this.btnmodificaclasse.TabIndex = 67;
            this.btnmodificaclasse.Text = "Modifica";
            this.btnmodificaclasse.UseVisualStyleBackColor = true;
            this.btnmodificaclasse.Click += new System.EventHandler(this.btnmodificaclasse_Click);
            // 
            // btnaggiungiclasse
            // 
            this.btnaggiungiclasse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiungiclasse.Location = new System.Drawing.Point(389, 90);
            this.btnaggiungiclasse.Name = "btnaggiungiclasse";
            this.btnaggiungiclasse.Size = new System.Drawing.Size(95, 33);
            this.btnaggiungiclasse.TabIndex = 66;
            this.btnaggiungiclasse.Text = "Aggiungi";
            this.btnaggiungiclasse.UseVisualStyleBackColor = true;
            this.btnaggiungiclasse.Click += new System.EventHandler(this.btnaggiungiclasse_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(553, 90);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 19);
            this.label15.TabIndex = 50;
            this.label15.Text = "Sezione:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(549, 123);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 19);
            this.label14.TabIndex = 49;
            this.label14.Text = "Indirizzo:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(-66, 286);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 19);
            this.label22.TabIndex = 50;
            this.label22.Text = "Utenti";
            // 
            // cmbindirizzo
            // 
            this.cmbindirizzo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbindirizzo.FormattingEnabled = true;
            this.cmbindirizzo.Items.AddRange(new object[] {
            "Informatica",
            "Elettronica",
            "Mat",
            "Automazione",
            "Moda",
            "Meccatronica"});
            this.cmbindirizzo.Location = new System.Drawing.Point(647, 124);
            this.cmbindirizzo.Name = "cmbindirizzo";
            this.cmbindirizzo.Size = new System.Drawing.Size(106, 21);
            this.cmbindirizzo.TabIndex = 48;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(-66, 432);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 19);
            this.label23.TabIndex = 51;
            this.label23.Text = "Classi";
            // 
            // pnlclassi
            // 
            this.pnlclassi.Controls.Add(this.tbsigla);
            this.pnlclassi.Controls.Add(this.label1);
            this.pnlclassi.Controls.Add(this.btnaggiorna);
            this.pnlclassi.Controls.Add(this.btnelimina);
            this.pnlclassi.Controls.Add(this.btnmodificaclasse);
            this.pnlclassi.Controls.Add(this.btnaggiungiclasse);
            this.pnlclassi.Controls.Add(this.lsvClassi);
            this.pnlclassi.Controls.Add(this.label17);
            this.pnlclassi.Controls.Add(this.btnsalvaaula);
            this.pnlclassi.Controls.Add(this.label16);
            this.pnlclassi.Controls.Add(this.cmbistituto);
            this.pnlclassi.Controls.Add(this.tbsezione);
            this.pnlclassi.Controls.Add(this.label15);
            this.pnlclassi.Controls.Add(this.label14);
            this.pnlclassi.Controls.Add(this.cmbindirizzo);
            this.pnlclassi.Controls.Add(this.label13);
            this.pnlclassi.Controls.Add(this.cmbplessoaula);
            this.pnlclassi.Controls.Add(this.nupanno);
            this.pnlclassi.Controls.Add(this.label12);
            this.pnlclassi.Controls.Add(this.tbaula);
            this.pnlclassi.Controls.Add(this.label11);
            this.pnlclassi.Location = new System.Drawing.Point(12, 128);
            this.pnlclassi.Name = "pnlclassi";
            this.pnlclassi.Size = new System.Drawing.Size(774, 395);
            this.pnlclassi.TabIndex = 49;
            this.pnlclassi.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlclassi_Paint);
            // 
            // tbsigla
            // 
            this.tbsigla.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbsigla.Location = new System.Drawing.Point(647, 31);
            this.tbsigla.Name = "tbsigla";
            this.tbsigla.Size = new System.Drawing.Size(106, 22);
            this.tbsigla.TabIndex = 72;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(583, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 19);
            this.label1.TabIndex = 71;
            this.label1.Text = "Sigla";
            // 
            // btnaggiorna
            // 
            this.btnaggiorna.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiorna.Location = new System.Drawing.Point(389, 22);
            this.btnaggiorna.Name = "btnaggiorna";
            this.btnaggiorna.Size = new System.Drawing.Size(95, 33);
            this.btnaggiorna.TabIndex = 70;
            this.btnaggiorna.Text = "Aggiorna";
            this.btnaggiorna.UseVisualStyleBackColor = true;
            this.btnaggiorna.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnsalvaaula
            // 
            this.btnsalvaaula.BackColor = System.Drawing.Color.Transparent;
            this.btnsalvaaula.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalvaaula.BackgroundImage")));
            this.btnsalvaaula.Location = new System.Drawing.Point(662, 278);
            this.btnsalvaaula.Name = "btnsalvaaula";
            this.btnsalvaaula.Size = new System.Drawing.Size(32, 32);
            this.btnsalvaaula.TabIndex = 54;
            this.btnsalvaaula.Text = " ";
            this.btnsalvaaula.UseVisualStyleBackColor = false;
            this.btnsalvaaula.Click += new System.EventHandler(this.btnsalvaaula_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(508, 189);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(122, 19);
            this.label13.TabIndex = 47;
            this.label13.Text = "Punto Vendita:";
            // 
            // cmbplessoaula
            // 
            this.cmbplessoaula.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbplessoaula.FormattingEnabled = true;
            this.cmbplessoaula.Location = new System.Drawing.Point(647, 190);
            this.cmbplessoaula.Name = "cmbplessoaula";
            this.cmbplessoaula.Size = new System.Drawing.Size(106, 21);
            this.cmbplessoaula.TabIndex = 46;
            // 
            // nupanno
            // 
            this.nupanno.Location = new System.Drawing.Point(647, 61);
            this.nupanno.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nupanno.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupanno.Name = "nupanno";
            this.nupanno.Size = new System.Drawing.Size(106, 20);
            this.nupanno.TabIndex = 45;
            this.nupanno.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(574, 61);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 19);
            this.label12.TabIndex = 44;
            this.label12.Text = "Anno:";
            // 
            // tbaula
            // 
            this.tbaula.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbaula.Location = new System.Drawing.Point(647, 156);
            this.tbaula.Name = "tbaula";
            this.tbaula.Size = new System.Drawing.Size(106, 22);
            this.tbaula.TabIndex = 43;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(562, 159);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 19);
            this.label11.TabIndex = 42;
            this.label11.Text = "N. Aula:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(-65, 379);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 50);
            this.button1.TabIndex = 48;
            this.button1.Text = " ";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btnalunni
            // 
            this.btnalunni.BackColor = System.Drawing.Color.Transparent;
            this.btnalunni.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnalunni.BackgroundImage")));
            this.btnalunni.Location = new System.Drawing.Point(-65, 233);
            this.btnalunni.Name = "btnalunni";
            this.btnalunni.Size = new System.Drawing.Size(50, 50);
            this.btnalunni.TabIndex = 47;
            this.btnalunni.Text = " ";
            this.btnalunni.UseVisualStyleBackColor = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(12, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(239, 110);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 53;
            this.pictureBox3.TabStop = false;
            // 
            // FrmClassi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 535);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.pnlclassi);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnalunni);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmClassi";
            this.Text = "FrmClassi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmClassi_FormClosing);
            this.Load += new System.EventHandler(this.FrmClassi_Load);
            this.SizeChanged += new System.EventHandler(this.FrmClassi_SizeChanged);
            this.pnlclassi.ResumeLayout(false);
            this.pnlclassi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupanno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView lsvClassi;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnsalvaaula;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbistituto;
        private System.Windows.Forms.TextBox tbsezione;
        private System.Windows.Forms.Button btnelimina;
        private System.Windows.Forms.Button btnmodificaclasse;
        private System.Windows.Forms.Button btnaggiungiclasse;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cmbindirizzo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel pnlclassi;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbplessoaula;
        private System.Windows.Forms.NumericUpDown nupanno;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbaula;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnalunni;
        private System.Windows.Forms.Button btnaggiorna;
        private System.Windows.Forms.TextBox tbsigla;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}