﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Net;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmUtentiAdm : Form
    {
        #region VARIABILI GLOBALI

        List<ClsUtente> utenti = new List<ClsUtente>(); // XML
        List<ClsClasse> classi = new List<ClsClasse>();
        ClsUtente utente;
        private const String filename = "items.xml";
        bool salvaOmodifica;
        bool modifica = false;

        #endregion

        public FrmUtentiAdm()
        {
            InitializeComponent();

        }

        #region METODI

        private void Pulisci()
        {
            tbnome.Enabled = false;
            tbcognome.Enabled = false;
            tbemail.Enabled = false;
            rbAmministratore.Enabled = false;
            rbStudente.Enabled = false;
            rbPersonaleScolastico.Enabled = false;
            rbMerendero.Enabled = false;
            cbclasse.Enabled = false;
            btnsalvautente.Enabled = false;
            clearTbNome.Enabled = false;
            clearTbCognome.Enabled = false;
            clearTbEmail.Enabled = false;
            clearChecked.Enabled = false;
            btClearClasseDx.Enabled = false;

            tbnome.Clear();
            tbcognome.Clear();
            tbemail.Clear();
            rbAmministratore.Checked = false;
            rbStudente.Checked = false;
            rbPersonaleScolastico.Checked = false;
            rbMerendero.Checked = false;
            cbclasse.ResetText();
            cbclasse.SelectedIndex = -1;
            btnsalvautente.Enabled = false;

        }

        private void Aggiorna(ClsUtente utente)
        {
            utenti = utente.GetUtenti();
            lsvalunni.Items.Clear();
            if (utenti != null)
            {
                foreach (ClsUtente item in utenti)
                {
                    ListViewItem lvi = new ListViewItem(item.nome);
                    lvi.Tag = item.ID;
                    lvi.SubItems.Add(item.cognome);
                    lvi.SubItems.Add(item.siglaClasse);
                    lvi.SubItems.Add(item.tipologia.ToString());
                    lvi.SubItems.Add(item.email);
                    lvi.SubItems.Add(item.merendino.ToString());
                    lvi.SubItems.Add(item.candidabile.ToString());
                    lvi.SubItems.Add(item.bloccato.ToString());
                    lsvalunni.Items.Add(lvi);
                }
            }
            lblNumUtenti.Text = "Numero utenti: " + lsvalunni.Items.Count.ToString();
        }

        private void loadClassi()
        {
            ClsClasse classe = new ClsClasse();
            classi = classe.GetClassi("");
        }

        private void caricaCb()
        {
            if(classi != null)
            {
                foreach (ClsClasse item in classi)
                {
                    cbclasse.Items.Add(item.Sigla);
                    cbClassiFIltro.Items.Add(item.Sigla);
                }
            }

            List<char> utntTipo = utenti.Select(o => o.tipologia).Distinct().ToList();

            foreach (char item in utntTipo)
            {
                cbTipologiaFiltro.Items.Add(item);
            }


        }

        private void mostraCampi()
        {
            tbnome.Enabled = true;
            tbcognome.Enabled = true;
            tbemail.Enabled = true;
            rbAmministratore.Enabled = true;
            rbStudente.Enabled = true;
            rbPersonaleScolastico.Enabled = true;
            rbMerendero.Enabled = true;
            btnsalvautente.Enabled = true;
            clearTbNome.Enabled = true;
            clearTbCognome.Enabled = true;
            clearTbEmail.Enabled = true;
            clearChecked.Enabled = true;
            btClearClasseDx.Enabled = true;
            cbBloccato.Enabled = true;
            cbCandidabile.Enabled = true;
        }

        private void mostraBt()
        {
            btnaggiungi.Enabled = true;
            btnelimina.Enabled = true;
            btnmodifica.Enabled = true;
        }

        #endregion

        #region XML

        public void importXML(string Path)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path);
            XmlElement xmlElement = xmlDoc.DocumentElement;

            foreach (XmlNode xmlAlunno in xmlElement.ChildNodes)
            {
                if (xmlAlunno.Name.Equals("alunno")) // Alunno
                {
                    ClsUtente clsUtente = new ClsUtente();
                    ClsClasse clsClasse = new ClsClasse();
                    clsUtente.tipologia = 'S';
                    clsUtente.candidabile = 1;
                    foreach (XmlNode xmlNode in xmlAlunno.ChildNodes)
                    {
                        switch (xmlNode.Name)
                        {
                            case "classe":
                                List<ClsClasse> clsClassi = clsClasse.GetClassi(xmlNode.InnerText);
                                if (clsClassi == null)
                                {
                                    clsClasse = new ClsClasse();
                                    clsClasse.Sigla = xmlNode.InnerText;
                                    clsClasse.Sezione = xmlNode.InnerText[1];
                                    clsClasse.Indirizzo = ""; // (ClsClasse.eIndirizzo)xmlNode.InnerText[0];
                                    clsClasse.PuntoVendita = 'V'; // (ClsClasse.ePlesso)xmlNode.InnerText[0];
                                    clsClasse.Istituto = xmlNode.InnerText[2];
                                    clsClasse.Anno = (int)xmlNode.InnerText[0] - 48;
                                    clsClasse.CreateClasse(); // TODO: aggiungere controllo che è andata a buon fine
                                }
                                clsUtente.siglaClasse = xmlNode.InnerText;
                                break;
                            case "email":
                                clsUtente.email = xmlNode.InnerText;
                                break;
                            case "nome":
                                clsUtente.nome = xmlNode.InnerText;
                                break;
                            case "cognome":
                                clsUtente.cognome = xmlNode.InnerText;
                                break;
                            case "tipologia":
                                clsUtente.tipologia = xmlNode.InnerText[0];
                                break;
                        }
                    }
                    try
                    {
                        clsUtente.CreateUtente();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message + "  "+ clsUtente.Nome);
                    }
                }

                if (xmlAlunno.Name.Equals("personale")) // personale scolastico
                {
                    ClsUtente clsPersonale = new ClsUtente();
                    clsPersonale.tipologia = 'P';
                    //clsPersonale.candidabile = 1;
                    foreach (XmlNode xmlNode in xmlAlunno.ChildNodes)
                    {
                        switch (xmlNode.Name)
                        {
                            case "email":
                                clsPersonale.email = xmlNode.InnerText;
                                break;
                            case "nome":
                                clsPersonale.nome = xmlNode.InnerText;
                                break;
                            case "cognome":
                                clsPersonale.cognome = xmlNode.InnerText;
                                break;
                            case "tipologia":
                                clsPersonale.tipologia = xmlNode.InnerText[0];
                                break;
                        }
                    }

                    try
                    {
                        clsPersonale.CreateUtente();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message + "  "+ clsUtente.Nome);
                    }
                }
            }
        }

        private void btnimportaalunni_Click(object sender, EventArgs e)
        {
            ClsUtente utente = new ClsUtente();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            ofd.Filter = "File XML (*.xml)|*.xml|Tutti i file (*.*)|*.*";
            ofd.FilterIndex = 1;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string _pathFile = ofd.FileName;
                //string _pathFile = "5CFM.xml";
                importXML(_pathFile);
                //loadUtenti();
                //StampaUtenti();
                Aggiorna(utente);
            }
        }

        #endregion

        #region EVENTI

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["FrmLogin"] != null)
            {
                Application.OpenForms["FrmLogin"].Show();
                this.Close();
            }
        }

        private void btnaggiungi_Click(object sender, EventArgs e)
        {
            salvaOmodifica = true;
            mostraCampi();
        }

        private void btAggiorna_Click(object sender, EventArgs e)
        {
            mostraBt();
            utente = new ClsUtente();
            /*cbMerendino.Enabled = false;
            cbCandidabile.Enabled = false;
            cbBloccato.Enabled = false;*/
            utente.siglaClasse = "";

            if (cbTipologiaFiltro.SelectedIndex < 0 && cbClassiFIltro.SelectedIndex >= 0)
            {
                //Aggiorna con le CLASSI
                utente.siglaClasse = cbClassiFIltro.Text; 
            }

            if (cbTipologiaFiltro.SelectedIndex >= 0 && cbClassiFIltro.SelectedIndex < 0)
            {
                //Aggiorna con TIPOLOGIA
                switch (cbTipologiaFiltro.Text)
                {
                    case "Amministratore": utente.tipologia = 'A'; break;

                    case "Studente": utente.tipologia = 'S'; break;

                    case "Personale": utente.tipologia = 'P'; break;

                    case "Merendero": utente.tipologia = 'M'; break;
                }
            }

            if (cbTipologiaFiltro.SelectedIndex >= 0 && cbClassiFIltro.SelectedIndex >= 0)
            {
                //Aggiorna con CLASSE e TIPOLOGIA
                utente.siglaClasse = cbClassiFIltro.Text;
                switch (cbTipologiaFiltro.Text)
                {
                    case "Amministratore": utente.tipologia = 'A'; break;

                    case "Studente": utente.tipologia = 'S'; break;

                    case "Personale": utente.tipologia = 'P'; break;

                    case "Merendero": utente.tipologia = 'M'; break;
                }
            }

            Aggiorna(utente);
        }

        private void btnmodifica_Click(object sender, EventArgs e)
        {
            //spunta checkbox in base ai dati (merendino, candidabile, bloccato) dell'utente selezionato 
            ListViewItem Selezionato = lsvalunni.SelectedItems[0];
            string mer = Selezionato.SubItems[5].Text;
            string cand = Selezionato.SubItems[6].Text;
            string bloc = Selezionato.SubItems[7].Text;
            if (mer == "1" && cand== "1" && bloc=="0")
            {
                cbMerendino.Checked = true;
                cbCandidabile.Checked = true;
                cbBloccato.Checked = false;
            }
            if (mer == "0" && cand == "1" && bloc == "0")
            {
                cbMerendino.Checked = false;
                cbCandidabile.Checked = true;
                cbBloccato.Checked = false;
            }
            if (mer == "0" && cand == "0" && bloc == "1")
            {
                cbMerendino.Checked = false;
                cbCandidabile.Checked = false;
                cbBloccato.Checked = true;
            }
            if (mer == "0" && cand == "0" && bloc == "0")
            {
                cbMerendino.Checked = false;
                cbCandidabile.Checked = false;
                cbBloccato.Checked = false;
            }

            salvaOmodifica = false;
            modifica = true;
            if (lsvalunni.SelectedItems.Count == 1)
            {
                utente = new ClsUtente();
                int ID = (int)lsvalunni.SelectedItems[0].Tag;
                utente = utenti.First(u => u.ID == ID);
                tbnome.Text = utente.nome;
                tbcognome.Text = utente.cognome;
                tbemail.Text = utente.email;
                cbclasse.Text = utente.siglaClasse;

                switch (utente.tipologia)
                {
                    case 'A': rbAmministratore.Checked = true; break;

                    case 'S': rbStudente.Checked = true; break;

                    case 'P': rbPersonaleScolastico.Checked = true; break;

                    case 'M': rbMerendero.Checked = true; break;

                    default:
                        rbAmministratore.Checked = false;
                        rbStudente.Checked = false;
                        rbPersonaleScolastico.Checked = false;
                        rbMerendero.Checked = false;
                        break;
                }
                mostraCampi();
            }
            else
            {
                MessageBox.Show("Devi selezionare una riga dalla list view.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnelimina_Click(object sender, EventArgs e)
        {
            if (lsvalunni.SelectedItems.Count == 1)
            {
                utente = new ClsUtente();
                int ID = (int)lsvalunni.SelectedItems[0].Tag;
                utente = utenti.First(u => u.ID == ID);
                char tipologia = utente.tipologia;
                DialogResult dr = MessageBox.Show("Sei sicuro di eliminare questo utente:" + utente.nome + " " + utente.cognome, "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    string msg = utente.deleteUtente();
                    MessageBox.Show(msg);
                    utente = new ClsUtente();
                    switch (tipologia)
                    {
                        case 'A': utente.tipologia = 'A'; break;

                        case 'S': utente.tipologia = 'S'; break;

                        case 'P': utente.tipologia = 'P'; break;

                        case 'M': utente.tipologia = 'M'; break;

                        default: utente.tipologia = 'A'; break;
                    }
                    Aggiorna(utente);
                }
            }
            else
            {
                MessageBox.Show("Devi selezionare una riga dalla list view.", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnsalvautente_Click(object sender, EventArgs e)
        {
            bool erroreInserimento = false;
            string msg;
            ClsClasse classe = new ClsClasse();
            ClsUtente utnt;
            if (!modifica)
            {
                utnt = new ClsUtente();
            }
            else
            {
                utnt = utente;
            }
            if (tbnome.Text == "")
            {
                MessageBox.Show("Per inserire un utente devi inserire il suo nome", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                erroreInserimento = true;
            }
            else
            {
                utnt.nome = tbnome.Text;
            }
            if (tbcognome.Text == "" && !erroreInserimento)
            {
                MessageBox.Show("Per inserire un utente devi inserire il suo cognome", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                erroreInserimento = true;
            }
            else
            {
                utnt.cognome = tbcognome.Text;
            }
            if (tbemail.Text == "" && !erroreInserimento)
            {
                MessageBox.Show("Per inserire un utente devi inserire la sua email", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                erroreInserimento = true;
            }
            else
            {
                utnt.email = tbemail.Text;
            }
            //utnt.Merendino = 0;
            if (cbBloccato.Checked && !cbCandidabile.Checked && !cbMerendino.Checked && !erroreInserimento)
            {
                utnt.bloccato = 1;
                utnt.candidabile = 0;
                utnt.merendino = 0;
            }
            if (!cbBloccato.Checked && cbCandidabile.Checked && !cbMerendino.Checked && !erroreInserimento)
            {
                utnt.bloccato = 0;
                utnt.candidabile = 1;
                utnt.merendino = 0;
            }
            if (!cbBloccato.Checked && cbCandidabile.Checked && cbMerendino.Checked && !erroreInserimento)
            {
                utnt.bloccato = 0;
                utnt.candidabile = 1;
                utnt.merendino = 1;
            }
            if (rbStudente.Checked && !erroreInserimento)
            {
                utnt.tipologia = 'S';
                if (cbclasse.Text == "")
                {
                    MessageBox.Show("Per inserire uno studente devi scegliere la sua classe", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    erroreInserimento = true;
                }
                else
                {
                    utnt.siglaClasse = cbclasse.Text;
                }
            }
            else if (rbAmministratore.Checked && !erroreInserimento)
            {
                utnt.tipologia = 'A';
            }
            else if (rbPersonaleScolastico.Checked && !erroreInserimento)
            {
                utnt.tipologia = 'P';
            }
            else if (rbMerendero.Checked && !erroreInserimento)
            {
                utnt.tipologia = 'M';
            }
            else if(!erroreInserimento)
            {
                MessageBox.Show("Per inserire un utente devi scegliere la sua tipologia", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                erroreInserimento = true;
            }
            if (!erroreInserimento)
            {
                if (salvaOmodifica)
                {
                    msg = utnt.CreateUtente();
                }
                else
                {
                    msg = utnt.updateUtente();
                    switch (utnt.tipologia)
                    {
                        case 'A': cbTipologiaFiltro.SelectedIndex = 0; break;
                        case 'S': cbTipologiaFiltro.SelectedIndex = 1; break;
                        case 'P': cbTipologiaFiltro.SelectedIndex = 2; break;
                        case 'M': cbTipologiaFiltro.SelectedIndex = 3; break;
                    }
                }
                utnt = new ClsUtente();
                utnt.tipologia = utente.tipologia;
                cbMerendino.Checked = false;
                cbCandidabile.Checked = false;
                cbBloccato.Checked = false;
                MessageBox.Show(msg);
                Aggiorna(utnt);
                btAggiorna.PerformClick();// con questo la visualizzazione dopo la modifica tiene conto dei filtri tipologia e classe
                modifica = false;
                Pulisci();
            }
            erroreInserimento = false;
        }

        private void btClearClasse_Click(object sender, EventArgs e)
        {
            cbClassiFIltro.SelectedIndex = -1;
            cbClassiFIltro.ResetText();
        }

        private void btClearTipologia_Click(object sender, EventArgs e)
        {
            cbTipologiaFiltro.SelectedIndex = -1;
            cbTipologiaFiltro.ResetText();
        }

        private void clearTbNome_Click(object sender, EventArgs e)
        {
            tbnome.Text = "";
        }

        private void clearTbCognome_Click(object sender, EventArgs e)
        {
            tbcognome.Text = "";
        }

        private void clearTbEmail_Click(object sender, EventArgs e)
        {
            tbemail.Text = "";
        }

        private void clearChecked_Click(object sender, EventArgs e)
        {
            rbStudente.Checked = false;
            rbAmministratore.Checked = false;
            rbPersonaleScolastico.Checked = false;
            rbMerendero.Checked = false;
        }

        #endregion

        #region USER CONTROL

        private void FrmUtentiAdm_Load(object sender, EventArgs e)
        {
            cbTipologiaFiltro.SelectedIndex = 1;
            loadClassi(); //to do
            caricaCb();

            ////user control
            //UcButtons ucb = new UcButtons();
            //ucb.Dock = DockStyle.Left; // Per riempire lo spazio di sinistra
            //ucb.BringToFront();  // Per portare i bottoni in primo piano
            //this.Controls.Add(ucb); // Per aggiungere lo usercontrol alla form
            ////panel2.SendToBack(); // Per assicurarsi che lo usercontrol non finisca sopra al panello del titolo
          

        }

        private void rbAmministratore_CheckedChanged(object sender, EventArgs e)
        {
            cbclasse.SelectedIndex = -1;
            cbclasse.ResetText();
            cbclasse.Enabled = false;
            utente.siglaClasse = null;
        }

        private void rbPersonaleScolastico_CheckedChanged(object sender, EventArgs e)
        {
            cbclasse.SelectedIndex = -1;
            cbclasse.ResetText();
            cbclasse.Enabled = false;
            utente.siglaClasse = null;
        }

        private void rbMerendero_CheckedChanged(object sender, EventArgs e)
        {
            cbclasse.SelectedIndex = -1;
            cbclasse.ResetText();
            cbclasse.Enabled = false;
            utente.siglaClasse = null;
        }

        private void rbStudente_CheckedChanged(object sender, EventArgs e)
        {
            cbclasse.Enabled = true;
        }


        //private void btnAdminis_Click(object sender, EventArgs e)
        //{
        //    FrmUtentiAdm f = new FrmUtentiAdm();
        //    this.Hide();
        //    f.Show();
        //}

        //private void label7_Click(object sender, EventArgs e)
        //{

        //}

        //private void label9_Click(object sender, EventArgs e)
        //{

        //}





        //private void btnClassi_Click_1(object sender, EventArgs e)
        //{
        //    FrmClassi f = new FrmClassi();
        //    this.Hide();
        //    f.Show();
        //}

        //private void btnMerendero_Click(object sender, EventArgs e)
        //{

        //}

        //private void btnProdotti_Click_1(object sender, EventArgs e)
        //{
        //    FrmProdotti f = new FrmProdotti();
        //    this.Hide();
        //    f.Show();
        //}

        //private void btnOrdini_Click(object sender, EventArgs e)
        //{
        //    FrmOrdini f = new FrmOrdini();
        //    this.Hide();
        //    f.Show();
        //}

        //private void btnOrdine_Click(object sender, EventArgs e)
        //{
        //    FrmOrdine f = new FrmOrdine();
        //    this.Hide();
        //    f.Show();
        //}

        #endregion

        private void FrmUtentiAdm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void btClearClasseDx_Click(object sender, EventArgs e)
        {
            cbclasse.SelectedIndex = -1;
            cbclasse.ResetText();
        }

        private void cbBloccato_CheckedChanged(object sender, EventArgs e)
        {
            if(cbBloccato.Checked==true)
            {
                cbMerendino.Checked = false;
                cbMerendino.Enabled = false;
                cbCandidabile.Checked = false;
                cbCandidabile.Enabled = false;
            }
            else
            {
                cbMerendino.Enabled = true;
                cbCandidabile.Enabled = true;
                //cbCandidabile.Checked = true;

            }
        }

        private void cbCandidabile_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCandidabile.Checked == false)
            {
                cbMerendino.Checked = false;
                cbMerendino.Enabled = false;
                cbBloccato.Checked = true;
            }
            else
            {
                cbMerendino.Enabled = true;
            }
        }
    }

}
