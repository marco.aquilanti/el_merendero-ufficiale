﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Oauth2.v2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
           
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void FormLoad()
        {
            //login avvenuto con successo 
            FrmScelta form = new FrmScelta();
            form.Show();
            this.Hide();
        }


        private void Login()
        {
            string[] Scopes = { "email", "profile" };
            UserCredential credential;
            Oauth2Service service;

            credential = GetUserCredential("credentials.json", "elmerendero-win", Scopes);
            service = GetService(credential);
            service.Userinfo.V2.Me.Get().Execute();

            //UserinfoResource userinfo = service.Userinfo;

            if (credential != null)
            {
                var oauthSerivce = new Google.Apis.Oauth2.v2.Oauth2Service(
                    new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "OAuth 2.0 Sample",
                    });


                var userinfo = service.Userinfo.Get().Execute();

                // Memorizzo le credenziali per le successive richieste
                // In locale
                Program.SetCredenziali(userinfo.Email, credential.Token.IdToken);
                // In remoto
                ClsUtente clsUserLogged = new ClsUtente();
                clsUserLogged.email = userinfo.Email;
                clsUserLogged.token = credential.Token.IdToken;
                clsUserLogged.UpdateUserToken();
                getID();
                FormLoad();
            }
        }


        public void getID()
        {
            List<ClsUtente> utenti = new List<ClsUtente>();
            ClsUtente utente = new ClsUtente();
            utente.tipologia = ' ';
            utenti = utente.GetUtenti();
            if(utenti != null)
            {
                foreach(ClsUtente item in utenti)
                {
                    if(item.email == Program.credenziali.Email)
                    {
                        Program.idUtente = item.ID;
                    }
                }
            }
            //MessageBox.Show(Program.idUtente.ToString());
        }

        public Oauth2Service GetOauth2Service(string clientSecretJson, string userName, string[] scopes)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrWhiteSpace(clientSecretJson))
                    throw new ArgumentNullException("clientSecretJson");
                if (!File.Exists(clientSecretJson))
                    throw new Exception("clientSecretJson file does not exist.");

                var cred = GetUserCredential(clientSecretJson, userName, scopes);

                return GetService(cred);
            }
            catch (Exception ex)
            {
                throw new Exception("Get Oauth2 service failed.", ex);
            }
        }

        private UserCredential GetUserCredential(string clientSecretJson, string userName, string[] scopes)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrWhiteSpace(clientSecretJson))
                    throw new ArgumentNullException("clientSecretJson");

                // inserisce il file credenziali se non presente
                if (!File.Exists(clientSecretJson)) 
                {
                    //throw new Exception("clientSecretJson file does not exist.");
                    StreamWriter sw = new StreamWriter("credentials.json");
                    sw.Write("{\"installed\":{\"client_id\":\"13308368610-sk1q7lhjeh58r3a3os1mjbf9k0tv309p.apps.googleusercontent.com\",\"project_id\":\"elmerendero-5c-web-login\",\"auth_uri\":\"https://accounts.google.com/o/oauth2/auth\",\"token_uri\":\"https://oauth2.googleapis.com/token\",\"auth_provider_x509_cert_url\":\"https://www.googleapis.com/oauth2/v1/certs\",\"client_secret\":\"4NVquD9BzdZhK3niZNN6eBEU\",\"redirect_uris\":[\"urn:ietf:wg:oauth:2.0:oob\",\"http://localhost\"]}}");
                    sw.Close();
                }

                // These are the scopes of permissions you need. It is best to request only what you need and not all of them               
                using (var stream = new FileStream(clientSecretJson, FileMode.Open, FileAccess.Read))
                {
                    string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    credPath = Path.Combine(credPath, ".credentials/", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);


                    // Requesting Authentication or loading previously stored authentication for userName
                    UserCredential credential = null;
                    var thread = new Thread(() =>
                            credential = (GoogleWebAuthorizationBroker.AuthorizeAsync(
                                GoogleClientSecrets.Load(stream).Secrets,
                                scopes,
                                userName,
                                CancellationToken.None,
                                new FileDataStore(credPath, true)).Result)
)
                    { IsBackground = false };

                    thread.Start();
                    if (!thread.Join(90000))
                    {
                        MessageBox.Show("Login Timeout");
                        Application.Exit();
                    }
                    {
                        //credential.GetAccessTokenForRequestAsync();
                        return credential;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Get user credentials failed.", ex);
            }
        }


    /// <summary>
    /// This method get a valid service
    /// </summary>
    /// <param name="credential">Authecated user credentail</param>
    /// <returns>Oauth2Service used to make requests against the Oauth2 API</returns>
    private Oauth2Service GetService(UserCredential credential)
        {
            try
            {
                if (credential == null)
                    throw new ArgumentNullException("credential");

                // Create Oauth2 API service.
                return new Oauth2Service(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "elmerendero-win"
                });
            }
            catch (Exception ex)
            {
                throw new Exception("Get Oauth2 service failed.", ex);
            }
        }

        private void login()
        {
            string[] Scopes = { "email", "profile" };
            UserCredential credential;
            Oauth2Service service;

            credential = GetUserCredential("credentials.json", "elmerendero-win", Scopes);
            service = GetService(credential);
            service.Userinfo.V2.Me.Get().Execute();

            //UserinfoResource userinfo = service.Userinfo;

            if (credential != null)
            {
                var oauthSerivce = new Google.Apis.Oauth2.v2.Oauth2Service(
                    new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "OAuth 2.0 Sample",
                    });

                var userinfo = service.Userinfo.Get().Execute();


                //var userInfo = await OauthSerivce.Userinfo.Get().ExecuteAsync();
                // You can use userInfo.Email, Gender, FamilyName, ... 
            }
        }

        private void btnChiusura_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
