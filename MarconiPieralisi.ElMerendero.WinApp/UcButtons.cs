﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class UcButtons : UserControl
    {
        public UcButtons()
        {
            InitializeComponent();
        }

        

        private void btnUtenti_Click(object sender, EventArgs e)
        {
            ParentForm.Close();
            FrmUtentiAdm f = new FrmUtentiAdm();
           
            f.Show();
        }

        private void btnOrdini_Click(object sender, EventArgs e)
        {

            ParentForm.Close();
            FrmOrdini f = new FrmOrdini();            
            f.Show();
        }

        private void btnClassi_Click(object sender, EventArgs e)
        {
            //string s = ParentForm.Name;
            //// Display the name in a message box.
            //MessageBox.Show("My Parent is " + s + ".");
            ParentForm.Close();
            FrmClassi f = new FrmClassi();
           
            f.Show();
        }

        private void btnProdotti_Click(object sender, EventArgs e)
        {
            ParentForm.Close();
            FrmProdotti f = new FrmProdotti();
            
            f.Show();
        }

        private void btnOrdine_Click(object sender, EventArgs e)
        {
            ParentForm.Close();
            FrmOrdine f = new FrmOrdine();
            
            f.Show();
        }

        private void pbxChiudi_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnPuntoVendita_Click(object sender, EventArgs e)
        {

        }

        private void btnCategoria_Click(object sender, EventArgs e)
        {

        }
    }
}
