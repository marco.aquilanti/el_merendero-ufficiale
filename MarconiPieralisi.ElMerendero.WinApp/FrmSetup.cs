﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmSetup : Form
    {
        public FrmSetup()
        {
            InitializeComponent();
        }
        private ClsSetup _setup = new ClsSetup();
        List<ClsSetup> lsetup;//se crea problemi devo metterlo nel load
        bool _edit = false;//edit setup

        #region Metodi
        private void ChiudiInput()
        {
            tbchiave.Enabled = false;
            tbvalore.Enabled = false;
            btnsalvasetup.Enabled = false;
        }
        private void ApriInput()
        {
            btnsalvasetup.Enabled = true;
            tbchiave.Enabled = true;
            tbvalore.Enabled = true;
        }

        private void PuliziaInput()
        {
            tbchiave.Clear();
            tbvalore.Clear();
        }

        private void LoadSetup()
        {
            ClsSetup setup = new ClsSetup();
            lsetup = setup.GetSetup();

            lsvSetup.Items.Clear();
            if (lsetup != null)
            {
                for (int i = 0; i < lsetup.Count; i++)
                {
                    ListViewItem lista = new ListViewItem(lsetup[i].chiave);
                    lista.SubItems.Add(Convert.ToString(lsetup[i].valore));
                    lsvSetup.Items.Add(lista);
                }
            }

        }
#endregion
        private void btnaggiungisetup_Click(object sender, EventArgs e)
        {
            ApriInput();
            PuliziaInput();
        }

        private void btneliminasetup_Click(object sender, EventArgs e)
        {
            if (lsvSetup.SelectedItems.Count >= 1)
            {
                ClsSetup setup = new ClsSetup();
                setup.chiave = lsvSetup.SelectedItems[0].Text;
                MessageBox.Show(setup.deleteSetup());
                LoadSetup();
            }
        }

        private void btget_Click(object sender, EventArgs e)
        {
            _setup.GetSetup();
        }

        private void FrmSetup_Load(object sender, EventArgs e)
        {
            
            LoadSetup();
            ChiudiInput();
        }

        private void btnmodificasetup_Click(object sender, EventArgs e)
        {
            ApriInput();
            tbchiave.Enabled = false;
            _edit = true;
            if (lsvSetup.SelectedIndices.Count == 1)
            {
                int indiceSelezionato = lsvSetup.SelectedIndices[0];
                if (indiceSelezionato >= 0)
                {
                    tbchiave.Text = lsetup[indiceSelezionato].chiave;
                    tbvalore.Text = lsetup[indiceSelezionato].valore;
                }
            }
            else
            {
                MessageBox.Show("svegliaaa");
            }
        }

        private void btaggiorna_Click(object sender, EventArgs e)
        {
            LoadSetup();
        }

        private void btnsalvasetup_Click(object sender, EventArgs e)
        {
            try
            {
                _setup.chiave = tbchiave.Text;
                _setup.valore = tbvalore.Text;
                if (_edit == false)
                {
                    string msg = _setup.createSetup();
                }
                else
                {
                    MessageBox.Show(_setup.updateSetup());
                    _edit = false;
                }
                PuliziaInput();
                ChiudiInput();
                LoadSetup();
            }
            catch (Exception)
            {
                MessageBox.Show("RICONTROLLA I CAMPI");
                throw;
            }
        }
    }
}
