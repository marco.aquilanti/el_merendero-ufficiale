﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmClassi : Form
    {
        public FrmClassi()
        {
            InitializeComponent();
        }
        private ClsClasse _classe = new ClsClasse();
        List<ClsClasse> lclassi;//se crea problemi devo metterno nel load
        bool _edit = false;//edit classe

        #region Metodi
        private void ChiudiInput()
        {
            tbaula.Enabled = false;
            tbsezione.Enabled = false;
            nupanno.Enabled = false;
            cmbindirizzo.Enabled = false;
            cmbistituto.Enabled = false;
            cmbplessoaula.Enabled = false;
            btnsalvaaula.Enabled = false;
            tbsigla.Enabled = false;
        }
        private void ApriInput()
        {
            tbaula.Enabled = true;
            tbsezione.Enabled = true;
            nupanno.Enabled = true;
            cmbindirizzo.Enabled = true;
            cmbistituto.Enabled = true;
            cmbplessoaula.Enabled = true;
            btnsalvaaula.Enabled = true;
            tbsigla.Enabled = true;
        }

        private void PuliziaInput()
        {
            tbsezione.Clear();
            tbsigla.Clear();
        }
        #endregion

        private void btnaggiungiclasse_Click(object sender, EventArgs e)
        {
            PuliziaInput();
            ApriInput();                      
        }

        private void pnlclassi_Paint(object sender, PaintEventArgs e)
        {
        }
        
        private void btnsalvaaula_Click(object sender, EventArgs e)
        {            
            try
            {
                _classe.NumeroAula = tbaula.Text;
                _classe.Sezione = Convert.ToChar(tbsezione.Text.ToUpper());
                _classe.Puntovendita = cmbplessoaula.SelectedItem.ToString()[0];              
                _classe.Istituto = cmbistituto.SelectedItem.ToString()[0];                
                _classe.Anno =Convert.ToInt32( nupanno.Value);
                _classe.Indirizzo = cmbindirizzo.SelectedItem.ToString();                
                _classe.Sigla= tbsigla.Text.ToUpper();
                if (_edit==false)
                {
                    string msg = _classe.CreateClasse();
                }
                else
                {                   
                   MessageBox.Show(_classe.UpdateClasse());
                    _edit = false;
                }
                PuliziaInput();
                ChiudiInput();
                LoadClassi();
            }
            catch (Exception)
            {
                MessageBox.Show("RICONTROLLA I CAMPI");
                throw;
            }

        }
        private void LoadClassi()
        {
            ClsClasse classe= new ClsClasse();
            //lclassi = classe.GetClassi2("",new ClsCredenziali("st7966@iismarconipieralisi.it","12345"));
            lclassi = classe.GetClassi("");

            lsvClassi.Items.Clear();
            if(lclassi != null)
            {
                for (int i = 0; i < lclassi.Count; i++)
                {
                    ListViewItem lista = new ListViewItem(lclassi[i].Sigla);
                    lista.SubItems.Add(Convert.ToString(lclassi[i].Anno));
                    lista.SubItems.Add(Convert.ToString(lclassi[i].Sezione));
                    lista.SubItems.Add(Convert.ToString(lclassi[i].Indirizzo));
                    lista.SubItems.Add(Convert.ToString(lclassi[i].NumeroAula));
                    lista.SubItems.Add(Convert.ToString(lclassi[i].Puntovendita));
                    lsvClassi.Items.Add(lista);
                }
            }
           
        }
        private void button2_Click(object sender, EventArgs e)
        {
            LoadClassi();
        }
        

        private void FrmClassi_Load(object sender, EventArgs e)
        {
            LoadClassi();
            ChiudiInput();
            cmbistituto.DataSource = Enum.GetValues(typeof(ClsClasse.eIstituto));
            cmbplessoaula.DataSource = Enum.GetValues(typeof(ClsClasse.ePuntoVendita));

            //user control
            //UcButtons ucb = new UcButtons();
            //ucb.Dock = DockStyle.Left; // Per riempire lo spazio di sinistra
            //ucb.BringToFront();  // Per portare i bottoni in primo piano
            //this.Controls.Add(ucb); // Per aggiungere lo usercontrol alla form
            //panel2.SendToBack(); // Per assicurarsi che lo usercontrol non finisca sopra al panello del titolo

        }

        private void btnelimina_Click(object sender, EventArgs e)
        {
            if (lsvClassi.SelectedItems.Count>=1)
            {
                ClsClasse classe = new ClsClasse();
                classe.Sigla = lsvClassi.SelectedItems[0].Text;             
                MessageBox.Show(classe.DeleteClasse());
                LoadClassi();
            }
        }

        private void btnmodificaclasse_Click(object sender, EventArgs e)
        {
            ApriInput();
            tbsigla.Enabled = false;
            _edit = true;
            if (lsvClassi.SelectedIndices.Count == 1)
            {
                int indiceSelezionato = lsvClassi.SelectedIndices[0];
                if (indiceSelezionato >= 0)
                {
                    tbsigla.Text = lclassi[indiceSelezionato].Sigla;
                    tbsezione.Text = Convert.ToString(lclassi[indiceSelezionato].Sezione);
                    tbaula.Text = Convert.ToString(lclassi[indiceSelezionato].NumeroAula);                    
                    cmbindirizzo.Text = lclassi[indiceSelezionato].Indirizzo.ToString();
                    cmbistituto.Text = lclassi[indiceSelezionato].Istituto.ToString();
                    cmbplessoaula.Text = lclassi[indiceSelezionato].Puntovendita.ToString();                    
                }
            }
            else
            {
                MessageBox.Show("svegliaaa");
            }
        }

        private void btnUtenti_Click(object sender, EventArgs e)
        {
            FrmUtentiAdm f = new FrmUtentiAdm();
            this.Hide();
            f.Show();
        }

        private void FrmClassi_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void FrmClassi_SizeChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("Width: " + this.Size.Width + ", Height: " + this.Size.Height, "Resize");
        }
    }
}
