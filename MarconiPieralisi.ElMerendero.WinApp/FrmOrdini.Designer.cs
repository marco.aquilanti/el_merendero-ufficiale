﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmOrdini
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btClearClasse = new System.Windows.Forms.Button();
            this.btnaggiungiordini = new System.Windows.Forms.Button();
            this.btnAggiorna = new System.Windows.Forms.Button();
            this.btnmodificaordini = new System.Windows.Forms.Button();
            this.btneliminaoridini = new System.Windows.Forms.Button();
            this.dtpData = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbClasse = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lsvProdotti = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btClearClasse);
            this.panel1.Controls.Add(this.btnaggiungiordini);
            this.panel1.Controls.Add(this.btnAggiorna);
            this.panel1.Controls.Add(this.btnmodificaordini);
            this.panel1.Controls.Add(this.btneliminaoridini);
            this.panel1.Controls.Add(this.dtpData);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.cmbClasse);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lsvProdotti);
            this.panel1.Location = new System.Drawing.Point(16, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(646, 368);
            this.panel1.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(489, 250);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 86);
            this.label2.TabIndex = 156;
            this.label2.Text = "STATO:\r\nI: In corso\r\nO: Ordinato\r\nP: Preparato\r\nR: Ritirato";
            // 
            // btClearClasse
            // 
            this.btClearClasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClearClasse.Location = new System.Drawing.Point(232, 37);
            this.btClearClasse.Name = "btClearClasse";
            this.btClearClasse.Size = new System.Drawing.Size(25, 23);
            this.btClearClasse.TabIndex = 155;
            this.btClearClasse.Text = "X";
            this.btClearClasse.UseVisualStyleBackColor = true;
            this.btClearClasse.Click += new System.EventHandler(this.btClearClasse_Click);
            // 
            // btnaggiungiordini
            // 
            this.btnaggiungiordini.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaggiungiordini.Location = new System.Drawing.Point(492, 95);
            this.btnaggiungiordini.Name = "btnaggiungiordini";
            this.btnaggiungiordini.Size = new System.Drawing.Size(116, 44);
            this.btnaggiungiordini.TabIndex = 154;
            this.btnaggiungiordini.Text = "Aggiungi";
            this.btnaggiungiordini.UseVisualStyleBackColor = true;
            this.btnaggiungiordini.Click += new System.EventHandler(this.btnaggiungiordini_Click);
            // 
            // btnAggiorna
            // 
            this.btnAggiorna.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAggiorna.Location = new System.Drawing.Point(492, 37);
            this.btnAggiorna.Name = "btnAggiorna";
            this.btnAggiorna.Size = new System.Drawing.Size(116, 44);
            this.btnAggiorna.TabIndex = 120;
            this.btnAggiorna.Text = "Aggiorna";
            this.btnAggiorna.UseVisualStyleBackColor = true;
            this.btnAggiorna.Click += new System.EventHandler(this.btnAggiorna_Click);
            // 
            // btnmodificaordini
            // 
            this.btnmodificaordini.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmodificaordini.Location = new System.Drawing.Point(492, 195);
            this.btnmodificaordini.Name = "btnmodificaordini";
            this.btnmodificaordini.Size = new System.Drawing.Size(116, 43);
            this.btnmodificaordini.TabIndex = 122;
            this.btnmodificaordini.Text = "Modifica";
            this.btnmodificaordini.UseVisualStyleBackColor = true;
            this.btnmodificaordini.Click += new System.EventHandler(this.btnmodificaordini_Click);
            // 
            // btneliminaoridini
            // 
            this.btneliminaoridini.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneliminaoridini.Location = new System.Drawing.Point(492, 145);
            this.btneliminaoridini.Name = "btneliminaoridini";
            this.btneliminaoridini.Size = new System.Drawing.Size(116, 43);
            this.btneliminaoridini.TabIndex = 123;
            this.btneliminaoridini.Text = "Elimina";
            this.btneliminaoridini.UseVisualStyleBackColor = true;
            this.btneliminaoridini.Click += new System.EventHandler(this.btneliminaoridini_Click);
            // 
            // dtpData
            // 
            this.dtpData.Location = new System.Drawing.Point(105, 66);
            this.dtpData.Name = "dtpData";
            this.dtpData.Size = new System.Drawing.Size(182, 20);
            this.dtpData.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(25, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 18);
            this.label11.TabIndex = 10;
            this.label11.Text = "Data:";
            // 
            // cmbClasse
            // 
            this.cmbClasse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClasse.FormattingEnabled = true;
            this.cmbClasse.Location = new System.Drawing.Point(105, 38);
            this.cmbClasse.Name = "cmbClasse";
            this.cmbClasse.Size = new System.Drawing.Size(121, 21);
            this.cmbClasse.TabIndex = 7;
            this.cmbClasse.SelectedIndexChanged += new System.EventHandler(this.cmbClasse_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(25, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 18);
            this.label9.TabIndex = 6;
            this.label9.Text = "Classe:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(25, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 18);
            this.label8.TabIndex = 5;
            this.label8.Text = "Filtra per";
            // 
            // lsvProdotti
            // 
            this.lsvProdotti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader8,
            this.columnHeader1});
            this.lsvProdotti.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsvProdotti.FullRowSelect = true;
            this.lsvProdotti.HideSelection = false;
            this.lsvProdotti.Location = new System.Drawing.Point(28, 96);
            this.lsvProdotti.Name = "lsvProdotti";
            this.lsvProdotti.Size = new System.Drawing.Size(422, 241);
            this.lsvProdotti.TabIndex = 0;
            this.lsvProdotti.UseCompatibleStateImageBehavior = false;
            this.lsvProdotti.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "ID";
            this.columnHeader5.Width = 61;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Data";
            this.columnHeader6.Width = 114;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Stato";
            this.columnHeader8.Width = 68;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Punto vendita";
            this.columnHeader1.Width = 169;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 15.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 24);
            this.label1.TabIndex = 121;
            this.label1.Text = "Lista degli ordini";
            // 
            // FrmOrdini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 436);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "FrmOrdini";
            this.Text = "FrmOrdini";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOrdini_FormClosing);
            this.Load += new System.EventHandler(this.FrmOrdini_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbClasse;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView lsvProdotti;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button btnAggiorna;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnmodificaordini;
        private System.Windows.Forms.Button btneliminaoridini;
        private System.Windows.Forms.Button btnaggiungiordini;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button btClearClasse;
        private System.Windows.Forms.Label label2;
    }
}