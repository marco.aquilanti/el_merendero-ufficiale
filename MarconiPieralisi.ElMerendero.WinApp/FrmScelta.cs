﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmScelta : Form
    {
        public FrmScelta()
        {
            InitializeComponent();
        }

        private void puntiVenditaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.FrmPuntoVendita is null)
            {
                Program.FrmPuntoVendita = new FrmPuntoVendita();
            }
            mostraForm(Program.FrmPuntoVendita);
        }

        private void classiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.FrmClassi is null)
            {
                Program.FrmClassi = new FrmClassi();
            }
            mostraForm(Program.FrmClassi);
        }

        private void utentiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.FrmUtentiAdmin is null)
            {
                Program.FrmUtentiAdmin = new FrmUtentiAdm();
            }
            mostraForm(Program.FrmUtentiAdmin);
        }

        private void categoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.FrmCategoria is null)
            {
                Program.FrmCategoria = new FrmCategoria();
            }
            mostraForm(Program.FrmCategoria);
        }

        private void prodottiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Tolto l' if così dopo aggiorna combo box
            //if (Program.FrmProdotti is null)
            //{
                Program.FrmProdotti = new FrmProdotti();
            //}
            mostraForm(Program.FrmProdotti);
        }

        private void ordiniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.FrmOrdini is null)
            {
                Program.FrmOrdini = new FrmOrdini();
            }
            mostraForm(Program.FrmOrdini);
        }

        private void setupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.FrmSetup is null)
            {
                Program.FrmSetup = new FrmSetup();
            }
            mostraForm(Program.FrmSetup);
        }
        private void ordineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.FrmOrdine is null)
            {
                Program.FrmOrdine = new FrmOrdine();
            }
            mostraForm(Program.FrmOrdine);
        }
        private void mostraForm(Form form)
        {
            Form activeChild = this.ActiveMdiChild; //attualmente attivo
            form.MdiParent = this;
            if (activeChild != form || activeChild == null) //se il form child attivo è diverso da quello da cliccato entrare o se non ci sono form child attivi 
            {
                //prima nascondo il child per non fargli cambiare size nel form che devo mostrare
                if (ActiveMdiChild != null && activeChild!=null)
                {
                    activeChild.Hide();
                }
                if(activeChild != form) //se il form child attivo è diverso da quello da cliccato entrare 
                {
                    this.SetClientSizeCore(form.Size.Width - 12, form.Size.Height + 80); //valori offset in quanto constantemente aumenta la width di 12 e diminusice la height di 80
                }
                form.WindowState = FormWindowState.Maximized;
                form.Show();
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            logout();
            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
            }

        }

        private void logout()
        {
            string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            credPath = Path.Combine(credPath, ".credentials"); //, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

            if (System.IO.Directory.Exists(credPath))
            {
                try
                {
                    Directory.Delete(credPath, true);
                    FormLeave();
                }
                catch (Exception e)
                {
                    MessageBox.Show("The process failed: {0}", e.Message);
                }
            }
            else
                MessageBox.Show("La cartella {0} non esiste", credPath);
        }

        private void FormLeave()
        {
            FrmLogin login = new FrmLogin();
            login.Show();
            this.Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            //if (Program.FrmSetup is null)
            //{
            //    Program.FrmSetup = new FrmSetup();
            //}
            //mostraForm(Program.FrmSetup);
        }

        private void FrmScelta_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
