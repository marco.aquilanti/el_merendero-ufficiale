﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class UcButtons
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UcButtons));
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pbxChiudi = new System.Windows.Forms.PictureBox();
            this.btnCategoria = new System.Windows.Forms.Button();
            this.btnPuntoVendita = new System.Windows.Forms.Button();
            this.btnOrdine = new System.Windows.Forms.Button();
            this.btnProdotti = new System.Windows.Forms.Button();
            this.btnOrdini = new System.Windows.Forms.Button();
            this.btnUtenti = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.btnClassi = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbxChiudi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 221);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 19);
            this.label10.TabIndex = 127;
            this.label10.Text = "utenti";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 391);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 19);
            this.label7.TabIndex = 138;
            this.label7.Text = "Prodotti";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 570);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 19);
            this.label9.TabIndex = 140;
            this.label9.Text = "Ordine";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 146);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 19);
            this.label1.TabIndex = 123;
            this.label1.Text = "Classi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 487);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 16);
            this.label2.TabIndex = 142;
            this.label2.Text = "Ordini";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-4, 56);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 19);
            this.label3.TabIndex = 143;
            this.label3.Text = "Punto Vendita";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 296);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 19);
            this.label4.TabIndex = 145;
            this.label4.Text = "Categoria";
            // 
            // pbxChiudi
            // 
            this.pbxChiudi.Image = ((System.Drawing.Image)(resources.GetObject("pbxChiudi.Image")));
            this.pbxChiudi.Location = new System.Drawing.Point(12, 592);
            this.pbxChiudi.Name = "pbxChiudi";
            this.pbxChiudi.Size = new System.Drawing.Size(50, 50);
            this.pbxChiudi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxChiudi.TabIndex = 147;
            this.pbxChiudi.TabStop = false;
            this.pbxChiudi.Click += new System.EventHandler(this.pbxChiudi_Click);
            // 
            // btnCategoria
            // 
            this.btnCategoria.BackColor = System.Drawing.Color.Transparent;
            this.btnCategoria.BackgroundImage = global::MarconiPieralisi.ElMerendero.WinApp.Properties.Resources.categoria;
            this.btnCategoria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCategoria.Location = new System.Drawing.Point(26, 243);
            this.btnCategoria.Name = "btnCategoria";
            this.btnCategoria.Size = new System.Drawing.Size(50, 50);
            this.btnCategoria.TabIndex = 146;
            this.btnCategoria.Text = " ";
            this.btnCategoria.UseVisualStyleBackColor = false;
            this.btnCategoria.Click += new System.EventHandler(this.btnCategoria_Click);
            // 
            // btnPuntoVendita
            // 
            this.btnPuntoVendita.BackColor = System.Drawing.Color.Transparent;
            this.btnPuntoVendita.BackgroundImage = global::MarconiPieralisi.ElMerendero.WinApp.Properties.Resources.puntovendita;
            this.btnPuntoVendita.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPuntoVendita.Location = new System.Drawing.Point(20, 3);
            this.btnPuntoVendita.Name = "btnPuntoVendita";
            this.btnPuntoVendita.Size = new System.Drawing.Size(50, 50);
            this.btnPuntoVendita.TabIndex = 144;
            this.btnPuntoVendita.Text = " ";
            this.btnPuntoVendita.UseVisualStyleBackColor = false;
            this.btnPuntoVendita.Click += new System.EventHandler(this.btnPuntoVendita_Click);
            // 
            // btnOrdine
            // 
            this.btnOrdine.BackColor = System.Drawing.Color.Transparent;
            this.btnOrdine.BackgroundImage = global::MarconiPieralisi.ElMerendero.WinApp.Properties.Resources.iconaordini;
            this.btnOrdine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOrdine.Location = new System.Drawing.Point(10, 517);
            this.btnOrdine.Name = "btnOrdine";
            this.btnOrdine.Size = new System.Drawing.Size(60, 50);
            this.btnOrdine.TabIndex = 141;
            this.btnOrdine.Text = " ";
            this.btnOrdine.UseVisualStyleBackColor = false;
            this.btnOrdine.Click += new System.EventHandler(this.btnOrdine_Click);
            // 
            // btnProdotti
            // 
            this.btnProdotti.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProdotti.BackgroundImage")));
            this.btnProdotti.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnProdotti.Location = new System.Drawing.Point(13, 328);
            this.btnProdotti.Name = "btnProdotti";
            this.btnProdotti.Size = new System.Drawing.Size(60, 60);
            this.btnProdotti.TabIndex = 139;
            this.btnProdotti.Text = " ";
            this.btnProdotti.UseVisualStyleBackColor = true;
            this.btnProdotti.Click += new System.EventHandler(this.btnProdotti_Click);
            // 
            // btnOrdini
            // 
            this.btnOrdini.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOrdini.BackgroundImage")));
            this.btnOrdini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnOrdini.Location = new System.Drawing.Point(10, 424);
            this.btnOrdini.Name = "btnOrdini";
            this.btnOrdini.Size = new System.Drawing.Size(60, 60);
            this.btnOrdini.TabIndex = 137;
            this.btnOrdini.Text = " ";
            this.btnOrdini.UseVisualStyleBackColor = true;
            this.btnOrdini.Click += new System.EventHandler(this.btnOrdini_Click);
            // 
            // btnUtenti
            // 
            this.btnUtenti.BackColor = System.Drawing.Color.Transparent;
            this.btnUtenti.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUtenti.BackgroundImage")));
            this.btnUtenti.Location = new System.Drawing.Point(18, 168);
            this.btnUtenti.Name = "btnUtenti";
            this.btnUtenti.Size = new System.Drawing.Size(58, 50);
            this.btnUtenti.TabIndex = 136;
            this.btnUtenti.Text = " ";
            this.btnUtenti.UseVisualStyleBackColor = false;
            this.btnUtenti.Click += new System.EventHandler(this.btnUtenti_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(-492, 89);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(50, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 130;
            this.pictureBox6.TabStop = false;
            // 
            // btnClassi
            // 
            this.btnClassi.BackColor = System.Drawing.Color.Transparent;
            this.btnClassi.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClassi.BackgroundImage")));
            this.btnClassi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClassi.Location = new System.Drawing.Point(17, 87);
            this.btnClassi.Name = "btnClassi";
            this.btnClassi.Size = new System.Drawing.Size(59, 56);
            this.btnClassi.TabIndex = 122;
            this.btnClassi.Text = " ";
            this.btnClassi.UseVisualStyleBackColor = false;
            this.btnClassi.Click += new System.EventHandler(this.btnClassi_Click);
            // 
            // UcButtons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pbxChiudi);
            this.Controls.Add(this.btnCategoria);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnPuntoVendita);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnOrdine);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnProdotti);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnOrdini);
            this.Controls.Add(this.btnUtenti);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClassi);
            this.Name = "UcButtons";
            this.Size = new System.Drawing.Size(110, 649);
            ((System.ComponentModel.ISupportInitialize)(this.pbxChiudi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button btnUtenti;
        private System.Windows.Forms.Button btnOrdini;
        private System.Windows.Forms.Button btnProdotti;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnOrdine;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnClassi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPuntoVendita;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCategoria;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pbxChiudi;
    }
}
