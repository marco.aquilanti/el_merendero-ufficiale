﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmProdotti :Form
    {
        int indiceSelezionato, identificativo;
        public FrmProdotti()
        {
            
            InitializeComponent();
            disattiva();
            LoadCategoria();
        }
        
        private ClsProdotto prodotto = new ClsProdotto();
        List<ClsProdotto> listaProdotti;
        bool edit=false;
        List<ClsCategoria> lcategoria;//se crea problemi devo metterlo nel load

        private void loadProdotti()
        {
            ClsProdotto prodotto = new ClsProdotto();
            listaProdotti = prodotto.getProdotti();
            if (listaProdotti != null)
            {
                int ciao = listaProdotti.Count;
                lsvprodotti.Items.Clear();
                if (listaProdotti != null)
                {
                    foreach (ClsProdotto item in listaProdotti)
                    {
                        ListViewItem lvi = new ListViewItem(item.nome);
                        lvi.SubItems.Add(Convert.ToString(item.quantitamax));
                        lvi.SubItems.Add(item.descrizione);
                        lvi.SubItems.Add(Convert.ToString(item.prezzo));
                        lvi.SubItems.Add(Convert.ToString(item.IDCategoria));
                        lsvprodotti.Items.Add(lvi);
                    }
                }
            }   
        }
        private void attiva()
        {
            cmbCategoria.Enabled = true;
            txtnomeprodotto.Enabled = true;
            numQuantitàMax.Enabled = true;
            txtdescrizione.Enabled = true;
            numPrezzo.Enabled = true;
        }
        private void disattiva()
        {
            txtnomeprodotto.Enabled = false;
            numQuantitàMax.Enabled = false;
            txtdescrizione.Enabled = false;
            numPrezzo.Enabled = false;
        }
        
        private void btnaggiungi_Click(object sender, EventArgs e)
        {
            attiva();
            edit = false;
            //Commentato io
            //loadProdotti();
        }

        private void btnmodifica_Click(object sender, EventArgs e)
        {
            attiva();
            edit = true;
            if (lsvprodotti.SelectedIndices.Count == 1)
            {
                indiceSelezionato = lsvprodotti.SelectedIndices[0];
                if (indiceSelezionato >= 0)
                {
                    txtnomeprodotto.Text = listaProdotti[indiceSelezionato].nome;
                    numQuantita.Value = listaProdotti[indiceSelezionato].quantita;
                    numQuantitàMax.Value = listaProdotti[indiceSelezionato].quantitamax;
                    txtdescrizione.Text = listaProdotti[indiceSelezionato].descrizione;
                    numPrezzo.Text = listaProdotti[indiceSelezionato].prezzo.ToString();
                    //cmbCategoria = listaProdotti[indiceSelezionato].IDCategoria.ToString();
                    identificativo = listaProdotti[indiceSelezionato].ID;

                    //cmbCategoria.Text = lcategoria[indiceSelezionato].nome;
                    int indiceCategoria = lcategoria.FindIndex(app => app.ID == listaProdotti[indiceSelezionato].IDCategoria);
                    cmbCategoria.SelectedIndex = indiceCategoria;
                    //TODO cercare ID categoria dentro lcategoria per avere l'indice della combobox da selezionare
                }             
            }
            else
            {
                MessageBox.Show("Nessun prodotto selezionato per effettuare la modifica");
            }
        }

        private void btnelimina_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Sei sicuro?", "Eliminazione", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (lsvprodotti.SelectedItems.Count == 1)
                {
                    indiceSelezionato = lsvprodotti.SelectedIndices[0];
                    ClsProdotto prodotto = new ClsProdotto();
                    prodotto.nome = lsvprodotti.SelectedItems[0].Text;
                    prodotto.ID = listaProdotti[indiceSelezionato].ID;
                    string msg = prodotto.deleteProdotto();
                    if (msg == "503")
                    {
                        msg = "Impossibile eliminare un prodotto già ordinato!";
                    }
                    else
                    {
                        msg = "Prodotto eliminato correttamente";
                    }
                    MessageBox.Show(msg);
                    loadProdotti();
                }
            }
            
        }

        private void btnsalvautente_Click_1(object sender, EventArgs e)
        {
            //try
            //{
            //    prodotto.Nome = txtnomeprodotto.Text;
            //    prodotto.Quantità = Convert.ToInt32(numQuantità.Text);
            //    prodotto.Descrizione = txtdescrizione.Text;
            //    prodotto.Prezzo = Convert.ToSingle(numPrezzo.Text);
            //    prodotto.NomeCategoria = txtCategoria.Text;
            //    if (edit == false)
            //    {
            //        string msg = prodotto.createProdotto();
            //        loadProdotti();
            //    }
            //    else
            //    {
            //        string msg = prodotto.updateProdotto();
            //        loadProdotti();
            //        edit = false;
            //    }
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("RICONTROLLA I CAMPI");
            //    throw;
            //}
            //disattiva();
            try
            {
                
                prodotto.nome = txtnomeprodotto.Text;
                prodotto.quantitamax = Convert.ToInt32(numQuantitàMax.Text);
                prodotto.quantita = Convert.ToInt32(numQuantita.Text);//numQuantitaOrdinabile
                prodotto.descrizione = txtdescrizione.Text;
                prodotto.prezzo = Convert.ToSingle(numPrezzo.Text);
                prodotto.IDCategoria = Convert.ToInt32(lcategoria[cmbCategoria.SelectedIndex].ID);
                prodotto.IDMerendero = Program.idUtente;
                    //TODO recuperare ID utente
                if (!edit)
                {
                    string msg = prodotto.createProdotto();
                    MessageBox.Show("Prodotto creato");
                }
                else
                {
                    prodotto.ID = identificativo;
                    MessageBox.Show(prodotto.updateProdotto());
                    edit = false;
                }
                loadProdotti();
                disattiva();
            }
            catch (Exception)
            {
                MessageBox.Show("RICONTROLLA I CAMPI");
                throw;
            }
        }

        //private void lsvprodotti_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (lsvprodotti.SelectedItems.Count == 1)
        //    {
        //        txtnomeprodotto.Text = lsvprodotti.SelectedItems[0].Text;
        //        //txtquantità.Text = lsvprodotti.SelectedItems[0].SubItems[1].Text;
        //        txtdescrizione.Text = lsvprodotti.SelectedItems[0].SubItems[2].Text;
        //        //txtprezzo.Text = lsvprodotti.SelectedItems[0].SubItems[2].Text;
        //    }
        //}


        private void btnaggiorna_Click(object sender, EventArgs e)
        {
            loadProdotti();
        }

        private void FrmProdotti_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void FrmProdotti_Load(object sender, EventArgs e)
        {
            loadProdotti();
            LoadCategoria();
        }

        private void lsvprodotti_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LoadCategoria()
        {
            ClsCategoria categoria = new ClsCategoria();
            lcategoria = categoria.getCategorie();

            cmbCategoria.Items.Clear();
            if (lcategoria != null)
            {
                for (int i = 0; i < lcategoria.Count; i++)
                {
                    cmbCategoria.Items.Add(lcategoria[i].nome);
                }
            }
        }
    }
}
