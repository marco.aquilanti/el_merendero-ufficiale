﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{

    public partial class FrmCategoria : Form
    {
        int identificativo;
        int indiceSelezionato;
        public FrmCategoria()
        {
            InitializeComponent();
        }
        private ClsCategoria _categoria = new ClsCategoria();
        List<ClsCategoria> lcategoria;//se crea problemi devo metterlo nel load
        bool _edit = false;//edit puntivendita

        #region Metodi
        private void ChiudiInput()
        {
            txtnomeprodotto.Enabled = false;
            txtCategoria.Enabled = false;
        }
        private void ApriInput()
        {
            txtnomeprodotto.Enabled = true;
            txtCategoria.Enabled = true;
        }

        private void PuliziaInput()
        {
            txtnomeprodotto.Clear();
            txtCategoria.Clear();
        }

        private void LoadCategorie()
        {
            ClsCategoria categoria = new ClsCategoria();
            lcategoria = categoria.getCategorie();

            lsvprodotti.Items.Clear();
            if (lcategoria != null)
            {
                for (int i = 0; i < lcategoria.Count; i++)
                {
                    ListViewItem lista = new ListViewItem(lcategoria[i].ID.ToString());
                    lista.SubItems.Add(lcategoria[i].nome);
                    lista.SubItems.Add(Convert.ToString(lcategoria[i].imgpath));
                    lsvprodotti.Items.Add(lista);
                }
            }
        }
        #endregion

        private void FrmCategoria_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void btnaggiungi_Click(object sender, EventArgs e)
        {
            ApriInput();
        }

        private void FrmCategoria_Load(object sender, EventArgs e)
        {
            LoadCategorie();
            ChiudiInput();

            //user control
            //UcButtons ucb = new UcButtons();
            //ucb.Dock = DockStyle.Left; // Per riempire lo spazio di sinistra
            //ucb.BringToFront();  // Per portare i bottoni in primo piano
            //this.Controls.Add(ucb); // Per aggiungere lo usercontrol alla form
            //panel2.SendToBack(); // Per assicurarsi che lo usercontrol non finisca sopra al panello del titolo
        }

        private void btnmodifica_Click(object sender, EventArgs e)
        {
            ApriInput();
            _edit = true;
            if (lsvprodotti.SelectedIndices.Count == 1)
            {
                indiceSelezionato = lsvprodotti.SelectedIndices[0];
                if (indiceSelezionato >= 0)
                {
                    identificativo = lcategoria[indiceSelezionato].ID;
                    txtnomeprodotto.Text = lcategoria[indiceSelezionato].nome;
                    txtCategoria.Text = lcategoria[indiceSelezionato].imgpath;
                }
            }
            else
            {
                MessageBox.Show("svegliaaa");
            }
            LoadCategorie();
        }

        private void btnelimina_Click(object sender, EventArgs e)
        {
            indiceSelezionato = lsvprodotti.SelectedIndices[0];
            DialogResult dr = MessageBox.Show("Sei sicuro?", "Eliminazione", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (lsvprodotti.SelectedItems.Count >= 1)
                {
                    ClsCategoria categoria = new ClsCategoria();
                    categoria.ID = lcategoria[indiceSelezionato].ID;
                    MessageBox.Show(categoria.deleteCategoria());
                    LoadCategorie();
                }
            }
            LoadCategorie();

        }

        private void btnaggiorna_Click(object sender, EventArgs e)
        {
            LoadCategorie();
        }

        private void btnsalvautente_Click(object sender, EventArgs e)
        {
            try
            {
                _categoria.ID = identificativo;
                _categoria.nome = txtnomeprodotto.Text;
                _categoria.imgpath = txtCategoria.Text;
                if (!_edit)
                {
                    string msg = _categoria.createCategoria();
                }
                else
                {
                    MessageBox.Show(_categoria.updateCategoria());
                    _edit = false;
                }
                /*puliziainput();
                chiudiinput();*/
            }
            catch (Exception)
            {
                MessageBox.Show("ricontrolla i campi");
                throw;
            }
            LoadCategorie();
        }
    }
}

