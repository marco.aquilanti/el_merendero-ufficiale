using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public partial class FrmPuntoVendita : Form
    {
        public FrmPuntoVendita()
        {
            InitializeComponent();
        }
        private ClsPuntiVendita _puntivendita = new ClsPuntiVendita();
        List<ClsPuntiVendita> lpuntivendita;//se crea problemi devo metterlo nel load
        bool _edit = false;//edit puntivendita

        #region Metodi
        private void ChiudiInput()
        {
            tbnomeprodotto.Enabled = false;
            tbdescrizione.Enabled = false;
        }
        private void ApriInput()
        {
            tbnomeprodotto.Enabled = true;
            tbdescrizione.Enabled = true;
        }

        private void PuliziaInput()
        {
            tbnomeprodotto.Clear();
            tbdescrizione.Clear();
        }

        private void LoadPuntiVendita()
        {
            ClsPuntiVendita puntovendita = new ClsPuntiVendita();
            //lclassi = classe.GetClassi2("",new ClsCredenziali("st7966@iismarconipieralisi.it","12345"));
            lpuntivendita = puntovendita.GetPuntiVendita();

            lsvprodotti.Items.Clear();
            if (lpuntivendita != null)
            {
                for (int i = 0; i < lpuntivendita.Count; i++)
                {
                    ListViewItem lista = new ListViewItem(lpuntivendita[i].Nome);
                    lista.SubItems.Add(Convert.ToString(lpuntivendita[i].Descrizione));
                    lsvprodotti.Items.Add(lista);
                }
            }

        }
        #endregion

        private void FrmPuntoVendita_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void btnaggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                _puntivendita.Nome = tbnomeprodotto.Text;
                _puntivendita.Descrizione = tbdescrizione.Text;
                if (_edit == false)
                {
                    string msg = _puntivendita.createPuntiVendita();
                }
                else
                {
                    MessageBox.Show(_puntivendita.updatePuntiVendita());
                    _edit = false;
                }
                PuliziaInput();
                ChiudiInput();
                LoadPuntiVendita();
            }
            catch (Exception)
            {
                MessageBox.Show("RICONTROLLA I CAMPI");
                throw;
            }
        }

        private void FrmPuntoVendita_Load(object sender, EventArgs e)
        {
            LoadPuntiVendita();
            ChiudiInput();

            //user control
            //UcButtons ucb = new UcButtons();
            //ucb.Dock = DockStyle.Left; // Per riempire lo spazio di sinistra
            //ucb.BringToFront();  // Per portare i bottoni in primo piano
            //this.Controls.Add(ucb); // Per aggiungere lo usercontrol alla form
            //panel2.SendToBack(); // Per assicurarsi che lo usercontrol non finisca sopra al panello del titolo
        }

        private void btnmodifica_Click(object sender, EventArgs e)
        {
            ApriInput();
            _edit = true;
            if (lsvprodotti.SelectedIndices.Count == 1)
            {
                int indiceSelezionato = lsvprodotti.SelectedIndices[0];
                if (indiceSelezionato >= 0)
                {
<<<<<<< HEAD
                    tbnomeprodotto.Text = lpuntivendita[indiceSelezionato].Nome;
=======
                    tbnomeprodotto.Enabled = false;
                    tbnomeprodotto.Text =Convert.ToString( lpuntivendita[indiceSelezionato].Nome);
>>>>>>> master
                    tbdescrizione.Text = lpuntivendita[indiceSelezionato].Descrizione;
                }
            }
            else
            {
                MessageBox.Show("Nessun punto vendita selezionato per effettuare la modifica");
            }
        }

        private void btnelimina_Click(object sender, EventArgs e)
        {
<<<<<<< HEAD
            if (lsvprodotti.SelectedItems.Count >= 1)
            {
                ClsPuntiVendita puntivendita = new ClsPuntiVendita();
                puntivendita.Nome = lsvprodotti.SelectedItems[0].Text;
                MessageBox.Show(puntivendita.deletePuntiVendita());
                LoadPuntiVendita();
            }
=======
            if (lsvprodotti.SelectedIndices.Count == 1)
            {
                //conferma eliminazione
                DialogResult dialogResult = MessageBox.Show("Sicuro di voler eliminare il punto vendita?", "Conferma eliminazione", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (lsvprodotti.SelectedItems.Count >= 1)
                    {
                        ClsPuntiVendita puntivendita = new ClsPuntiVendita();
                        puntivendita.Nome = Convert.ToChar(lsvprodotti.SelectedItems[0].Text);
                        MessageBox.Show(puntivendita.deletePuntiVendita());
                        LoadPuntiVendita();

                    }
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Eliminazione annullata");
                }
            }
            else
            {
                MessageBox.Show("Nessun punto vendita selezionato per effettuare l'eliminazione");
            }


              
>>>>>>> master
        }

        private void btnaggiorna_Click(object sender, EventArgs e)
        {
            LoadPuntiVendita();
        }
    }
}
