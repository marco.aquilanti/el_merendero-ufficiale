using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon

namespace MarconiPieralisi.ElMerendero.WinApp
{
    class ClsClasse
    {
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws2/classi/";
        private const string WSPATH = Program.WSPATH + "classi/";
        public enum eIstituto
        {
            M,
            P
        }
        public enum ePuntoVendita
        {
            N,
            V
        }
        // Variabili
        private string _sigla;
        private char _sezione;
        private int _anno;
        private string _indirizzo;
        private char _puntovendita;
        private char _istituto;
        private string _numeroAula;
        // Proprietà
        public string Sigla { get => _sigla; set => _sigla = value; }
        public char Sezione { get => _sezione; set => _sezione = value; }
        public int Anno { get => _anno; set => _anno = value; }
        public string Indirizzo { get => _indirizzo; set => _indirizzo = value; }
        public char Puntovendita { get => _puntovendita; set => _puntovendita = value; }
        public char Istituto { get => _istituto; set => _istituto = value; }
        public string NumeroAula { get => _numeroAula; set => _numeroAula = value; }

        public ClsClasse()
        {
            Sigla = "";
            Sezione = ' ';

            Indirizzo = "";
            Puntovendita = ' ';
            Istituto = ' ';
            NumeroAula = "";
        }
        public ClsClasse(string sigla, char sezione, int anno, string indirizzo, char puntovendita, char istituto, string numeroAula)
        {
            Sigla = sigla;
            Sezione = sezione;
            Anno = anno;
            Indirizzo = indirizzo;
            Puntovendita = puntovendita;
            Istituto = istituto;
            NumeroAula = numeroAula;
        }
        public List<ClsClasse> GetClassi()//ultimo
        {

            List<ClsClasse> classi = null;
            //serealizzo su stringa json
            ClsClasse classe = new ClsClasse();

            //string JsonStr = JsonConvert.SerializeObject(Program.classi, Formatting.Indented);

            //creo richiesta
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getClassi.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKi537.36(KHTML, like Gecko) Chrome/27.0.1453.94. Safari/537.36";

            //leggo 
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStrem = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStrem);
                    string JsonStr = reader.ReadToEnd();
                    classi = JsonConvert.DeserializeObject<List<ClsClasse>>(JsonStr);

                }
                response.Close();
            }
            catch (Exception)
            {
            }

            return classi;
        }
        public List<ClsClasse> GetClassi(string sigla)//ultimo sono shifat devo usare questo
        {

            List<ClsClasse> classi = null;
            //serealizzo su stringa json
            ClsClasse classe = new ClsClasse();
            classe.Sigla = sigla;

            string JsonStr = JsonConvert.SerializeObject(classe, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            //creo richiesta
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getClassi.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            //  request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKi537.36(KHTML, like Gecko) Chrome/27.0.1453.94. Safari/537.36";

            //creo lo stream
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            //leggo 
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStrem = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStrem);
                    JsonStr = reader.ReadToEnd();
                    classi = JsonConvert.DeserializeObject<List<ClsClasse>>(JsonStr);

                }
                response.Close();
            }
            catch (Exception ex)
            {
                string sMsg = ex.Message;
<<<<<<< HEAD
=======
                //MessageBox.Show(ex.Message);
>>>>>>> master
            }

            return classi;
        }

        public string CreateClasse()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);


            // Creo la richiesta al WebService
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postClasse.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";

            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            // Leggo la risposta
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
            }
            catch (Exception)
            {
<<<<<<< HEAD

=======
                //MessageBox.Show(ex.Message);
>>>>>>> master
                //throw;
            }


            return msg;
        }
        public string DeleteClasse()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deleteClasse.php");
            request.Method = "DELETE";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {


                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();


            }
            catch (Exception)
            {
<<<<<<< HEAD

=======
                //MessageBox.Show(ex.Message);
>>>>>>> master
                // throw;
            }

            return msg;

        }
        public string UpdateClasse()
        {

            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);//se crea problemi tolgo
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putClasse.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKi537.36(KHTML, like Gecko) Chrome/27.0.1453.94. Safari/537.36";

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //if (response.StatusCode == HttpStatusCode.OK) // 200
                if (response.StatusCode == HttpStatusCode.Created) // 201
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
<<<<<<< HEAD
            catch (Exception){}
=======
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
>>>>>>> master
            return msg;
        }
    }
}
