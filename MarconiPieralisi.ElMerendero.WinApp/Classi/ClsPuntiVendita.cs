﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon
namespace MarconiPieralisi.ElMerendero.WinApp
{
    class ClsPuntiVendita
    {
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws/puntiVendita/";
        private const string WSPATH = Program.WSPATH + "PuntiVendita/";

        #region variabili
        private string _nome;
        private string _descrizione;
        #endregion
        #region proprietà
        public string Nome { get => _nome; set => _nome = value; }
        public string Descrizione { get => _descrizione; set => _descrizione = value; }
        #endregion
        #region costruttore
        public ClsPuntiVendita()
        {

        }
        public ClsPuntiVendita(string nome, string descrizione)
        {
            Nome = nome;
            Descrizione = descrizione;
        }
        #endregion
        #region metodi
        public List<ClsPuntiVendita> GetPuntiVendita()
        {
            List<ClsPuntiVendita> puntiVendita = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getPuntiVendita.php");
                request.Method = "GET"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    string JsonStr = reader.ReadToEnd();                            // Legge i ch dello stream fino all'ultimo
                    puntiVendita = JsonConvert.DeserializeObject<List<ClsPuntiVendita>>(JsonStr);    // Deserializza la stringa JSON in una lista di Libro
                }
                response.Close();
            }
            catch (Exception ex)
            {

            }
            return puntiVendita;
        }

        public string createPuntiVendita()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);

            // Creo la richiesta al WebService
            try
            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postPuntiVendita.php");
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = JsonStr.Length;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                // Creo lo stream della richiesta e aggiungo la stringa JSon
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
            }
            catch (Exception ex)
            {

            }
            return msg;
        }

        public string deletePuntiVendita()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deletePuntiVendita.php");
                request.Method = "DELETE";
                request.ContentType = "application/json";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
            catch (Exception ex)
            {

            }
            return msg;
        }

        public string updatePuntiVendita()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putPuntiVendita.php");
                request.Method = "PUT";
                request.ContentType = "application/json";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
            catch (Exception ex)
            {

            }
            return msg;
        }
        #endregion
    }
}

