﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       
using System.IO;        
using Newtonsoft.Json;
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    class ClsProdotto
    {
        #region variabili
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws/prodotti/";
        private const string WSPATH = Program.WSPATH + "prodotti/";
        private int _id;
        private string _nome;
        private string _descrizione;
        private int _quantita;
        private int _quantitamax;
        private float _prezzo;
        private int _categoria;
        private int _merendero;
        #endregion 
        #region proprietà
        //I nomi delle proprietà devono essere uguali a quelli del WS
        public string nome { get => _nome; set => _nome = value; }
        public string descrizione { get => _descrizione; set => _descrizione = value; }
        public int quantita { get => _quantita; set => _quantita = value; }
        public float prezzo { get => _prezzo; set => _prezzo = value; }
        public int IDCategoria { get => _categoria; set => _categoria = value; }
        public int ID { get => _id; set => _id = value; }
        public int quantitamax { get => _quantitamax; set => _quantitamax = value; }
        public int IDMerendero { get => _merendero; set => _merendero = value; }

        #endregion proprietà
        #region costruttore
        public ClsProdotto() { }
        public ClsProdotto(string nome, string descrizione, int quantita, float prezzo, string categoria, string id, string quantitamax, string merendero)
        {
            ID = Convert.ToInt32(id);
            this.nome = nome;
            this.descrizione = descrizione;
            this.quantita = quantita;
            this.quantitamax = Convert.ToInt32(quantitamax);
            this.prezzo = prezzo;
            IDCategoria = Convert.ToInt32(categoria);
            IDMerendero = Convert.ToInt32(merendero);
        }
        #endregion
        #region metodi
        public List<ClsProdotto> getProdotti()
        {

            List<ClsProdotto> prodottii = null;
            //serealizzo su stringa json
            ClsProdotto classe = new ClsProdotto();
            //Serializzo solo l' ID

            string JsonStr = JsonConvert.SerializeObject(classe.ID, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            //creo richiesta
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getProdotti.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            //  request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKi537.36(KHTML, like Gecko) Chrome/27.0.1453.94. Safari/537.36";

            //creo lo stream
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStrem = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStrem);
                    JsonStr = reader.ReadToEnd();
                    prodottii = JsonConvert.DeserializeObject<List<ClsProdotto>>(JsonStr);

                }
                response.Close();
            }
            catch (Exception)
            {
                throw;
            }

            return prodottii;
        }
        /*public List<ClsProdotto> getProdotti(string categoria)//ultimo
        {
            List<ClsProdotto> prodotti = null;
            //serealizzo su stringa json
            ClsProdotto prodotto= new ClsProdotto();
            prodotto.NomeCategoria = categoria;
            string JsonStr = JsonConvert.SerializeObject(prodotto, Formatting.Indented);

            //creo richiesta
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getClassi.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKi537.36(KHTML, like Gecko) Chrome/27.0.1453.94. Safari/537.36";

            //creo lo stream
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            //leggo 
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStrem = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStrem);
                JsonStr = reader.ReadToEnd();
                prodotti = JsonConvert.DeserializeObject<List<ClsProdotto>>(JsonStr);

            }
            response.Close();
            return prodotti;
        } */

        public string createProdotto()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);


            // Creo la richiesta al WebService
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postProdotto.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";

            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            // Leggo la risposta
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


            return msg;
        }

        public string deleteProdotto()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deleteProdotto.php");
            request.Method = "DELETE";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {


                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();


            }
            catch (Exception)
            {

                // throw;
            }

            return msg;
        }

        public string updateProdotto()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putProdotto.php");
            Console.WriteLine(request.Address);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKi537.36(KHTML, like Gecko) Chrome/27.0.1453.94. Safari/537.36";

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

            return msg;
        }
        #endregion

    }
}