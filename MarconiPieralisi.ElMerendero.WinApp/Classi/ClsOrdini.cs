using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    class ClsOrdini
    {
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws2/ordini/";
        private const string WSPATH = Program.WSPATH + "ordini/";

        public enum eStato
        {
            I,
            O
        }
        private int _id;
        private DateTime _data;
        private char _nomepuntovendita;
        private string _nomeprodotto;
        private string _classe;
        private eStato _stato;

        public string siglaClasse { get => _classe; set => _classe = value; }
        public string Nomeprodotto { get => _nomeprodotto; set => _nomeprodotto = value; }
        public eStato stato { get => _stato; set => _stato = value; }
        public int ID { get => _id; set => _id = value; }
        public DateTime data { get => _data; set => _data = value; }
        public char nomepuntovendita { get => _nomepuntovendita; set => _nomepuntovendita = value; }

        public ClsOrdini()
        {
        }
        public ClsOrdini(char Nomepuntovendita, string classe, eStato Stato, string id, string Data)
        {
            ID = Convert.ToInt32(id);
            data = Convert.ToDateTime(Data);
            stato = Stato;
            nomepuntovendita = Nomepuntovendita;
            siglaClasse = classe;
        }
        public List<ClsOrdini> getOrdini()
        {
            
            List<ClsOrdini> ordini = null;

            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getOrdini.php");
                request.Method = "POST"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.ContentType = "application/json";
                request.ContentLength = JsonStr.Length;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                // Creo lo stream della richiesta e aggiungo la stringa JSon
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    ordini = JsonConvert.DeserializeObject<List<ClsOrdini>>(JsonStr);    // Deserializza la stringa JSON in una list
                }
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return ordini;
        }

        public string createOrdine()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            // Creo la richiesta al WebService
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postOrdine.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }

        public string deleteOrdine()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            // Creo la richiesta al WebService
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deleteOrdine.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }

        public string updateOrdine()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putOrdini.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }

        public List<ClsOrdini> CercaOrdineData(DateTime data)
        {
            List<ClsOrdini> ordini = null;

            //string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            string JsonStr = "{\"data\": \""+ data.Year + "-" + data.Month + "-" + data.Day + "\"}";
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "CercaOrdineData.php");
                request.Method = "POST"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.ContentType = "application/json";
                request.ContentLength = JsonStr.Length;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                // Creo lo stream della richiesta e aggiungo la stringa JSon
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    ordini = JsonConvert.DeserializeObject<List<ClsOrdini>>(JsonStr);    // Deserializza la stringa JSON in una list
                }
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return ordini;
        }

        public List<ClsOrdini> CercaOrdineClasse()
        {
            List<ClsOrdini> ordini = null;

            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "CercaOrdineClasse.php");
                request.Method = "POST"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.ContentType = "application/json";
                request.ContentLength = JsonStr.Length;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                // Creo lo stream della richiesta e aggiungo la stringa JSon
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    ordini = JsonConvert.DeserializeObject<List<ClsOrdini>>(JsonStr);    // Deserializza la stringa JSON in una list
                }
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return ordini;
        }
    }
}