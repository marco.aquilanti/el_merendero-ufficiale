﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    class ClsOrdinare
    {
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws2/ordinare/";
        private const string WSPATH = Program.WSPATH + "ordinare/";

        private int _idutente;
        private int _idordine;

        public int Idutente { get => _idutente; set => _idutente = value; }
        public int Idordine { get => _idordine; set => _idordine = value; }

        public ClsOrdinare()
        {
        }
        public ClsOrdinare(string idutente, string idordine)
        {
            Idutente = Convert.ToInt32(idutente);
            Idordine = Convert.ToInt32(idordine);
        }
        public List<ClsOrdinare> getOrdini(string classe)
        {
            List<ClsOrdinare> ordinare = null;

            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getOrdine.php");
                request.Method = "POST"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.ContentType = "application/json";
                request.ContentLength = JsonStr.Length;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                // Creo lo stream della richiesta e aggiungo la stringa JSon
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    ordinare = JsonConvert.DeserializeObject<List<ClsOrdinare>>(JsonStr);    // Deserializza la stringa JSON in una lista di Libro
                }
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return ordinare;
        }

        public string createOrdinare()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            // Creo la richiesta al WebService
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postOrdine.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }

        public string deleteOrdinare()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            // Creo la richiesta al WebService
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deleteOrdine.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }


        public string updateOrdinare()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putOrdine.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }
    }
}
