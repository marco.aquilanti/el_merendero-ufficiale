﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon
namespace MarconiPieralisi.ElMerendero.WinApp
{
    class ClsPreferire
    {
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws/preferire/";
        private const string WSPATH = Program.WSPATH + "preferire/";

        #region variabili
        private int _id;
        private int _quantitaprodotto;
        private int _idutente;
        private int _idprodotto;
        #endregion
        #region proprietà
        public int Id { get => _id; set => _id = value; }
        public int Quantitaprodotto { get => _quantitaprodotto; set => _quantitaprodotto = value; }
        public int Idutente { get => _idutente; set => _idutente = value; }
        public int Idprodotto { get => _idprodotto; set => _idprodotto = value; }
        #endregion
        #region costruttore
        public ClsPreferire()
        {

        }
        public ClsPreferire(string id, string quantitaprodotto, string idutente, string idprodotto)
        {
            Id = Convert.ToInt32(id);
            Quantitaprodotto = Convert.ToInt32(quantitaprodotto);
            Idutente = Convert.ToInt32(idutente);
            Idprodotto = Convert.ToInt32(idprodotto);
        }
        #endregion
        #region metodi
        public List<ClsPreferire> GetPreferire()
        {
            List<ClsPreferire> preferire = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getPreferire.php");
                request.Method = "GET"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    string JsonStr = reader.ReadToEnd();                            // Legge i ch dello stream fino all'ultimo
                    preferire = JsonConvert.DeserializeObject<List<ClsPreferire>>(JsonStr);    // Deserializza la stringa JSON in una lista di Libro
                }
                response.Close();
            }
            catch (Exception ex)
            {

            }
            return preferire;
        }

        public string createPreferire()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);

            // Creo la richiesta al WebService
            try
            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postPreferire.php");
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = JsonStr.Length;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                // Creo lo stream della richiesta e aggiungo la stringa JSon
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
            }
            catch (Exception ex)
            {

            }
            return msg;
        }

        public string deletePreferire()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deletePreferire.php");
                request.Method = "DELETE";
                request.ContentType = "application/json";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
            catch (Exception ex)
            {

            }
            return msg;
        }

        public string updatePreferire()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putPreferire.php");
                request.Method = "PUT";
                request.ContentType = "application/json";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
            catch (Exception ex)
            {

            }
            return msg;
        }
        #endregion
    }
}
