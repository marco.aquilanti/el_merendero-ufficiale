using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon
//è questo
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public class ClsUtente
    {
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws2/utenti/";
        private const string WSPATH = Program.WSPATH + "utenti/";
        public enum eTipologia
        {
            A,//admin
            S,//studente
            P,//personale 
            M//merendero
        }

        private char[] vTipologia = { 'A', 'S', 'P', 'M' };


        #region VARIABILI

        int _ID;
        string _email;
        string _password;
        string _nome;
        string _cognome;
        char _tipologia;
        //DateTime _dataLogin;
        int _merendino;//if merendino=1 merendino true
        int _candidabile;
        int _bloccato;
        string _token;
        string _siglaClasse;

        #endregion

        #region PROPRIETA

        public int ID { get => _ID; set => _ID = value; }
        public string email { get => _email; set => _email = value; }
        public string password { get => _password; set => _password = value; }
        public string nome { get => _nome; set => _nome = value; }
        public string cognome { get => _cognome; set => _cognome = value; }
        public char tipologia { get => _tipologia; set => _tipologia = value; }
        public int merendino { get => _merendino; set => _merendino = value; }  //1 merendino 0 no merendino
        public string siglaClasse { get => _siglaClasse; set => _siglaClasse = value; }
        public int candidabile { get => _candidabile; set => _candidabile = value; }
        public int bloccato { get => _bloccato; set => _bloccato = value; }
        public string token { get => _token; set => _token = value; }
        //public DateTime dataLogin { get => _dataLogin; set => _dataLogin = value; }

        #endregion

        #region COSTRUTTORI

        public ClsUtente()
        {
        }
        public ClsUtente(string id, string Email, string Password, string Nome, string Cognome, char Tipologia, int Merendino, int Candidabile, int Bloccato, string Token, string Classe)
        {
            ID = Convert.ToInt32(id);
            email = Email;
            password = Password;
            nome = Nome;
            cognome = Cognome;
            tipologia = Tipologia;
            merendino = Merendino;
            candidabile = Candidabile;
            bloccato = Bloccato;
            token = Token;
            //dataLogin = Convert.ToDateTime(DataLogin);
            siglaClasse = Classe;
        }

        #endregion

        #region METODI

        public List<ClsUtente> GetUtenti()
        {
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            List<ClsUtente> utenti = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getUtenti.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();//qui
                if (response.StatusCode == HttpStatusCode.OK)//200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                            // Legge i ch dello stream fino all'ultimo
                    utenti = JsonConvert.DeserializeObject<List<ClsUtente>>(JsonStr);
                }
                else if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    MessageBox.Show("Nessun utente trovato", "No content", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                response.Close();
                return utenti;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return null;
        }

        public string CreateUtente()
        {

            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            // Creo la richiesta al WebService
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postUtente.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();
            try
            {
                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }

        public string deleteUtente()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deleteUtente.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return msg;
        }

        public string updateUtente()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putUtente.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }

        public string UpdateUserToken()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr); // Non ancora valide perchè il DB non è stato aggiornato

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "loginUtente.php?password=ifl7oD0qxy");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
                return msg;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "";
        }
        #endregion

    }
}
