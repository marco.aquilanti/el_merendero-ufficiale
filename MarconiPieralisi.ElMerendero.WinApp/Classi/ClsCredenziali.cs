﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    public  class ClsCredenziali
    {
        private  string email;
        private  string token;

        public  string Email { get => email; set => email = value; } 
        public  string Token { get => token; set => token = value; }

        public ClsCredenziali()
        {

        }

        
    }
}
