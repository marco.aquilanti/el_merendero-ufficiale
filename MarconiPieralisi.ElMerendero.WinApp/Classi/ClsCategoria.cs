﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;       // Per HttpWebRequest e HttpWebResponse
using System.IO;        // Per lo Stream
using Newtonsoft.Json;  // Per JSon
using System.Windows.Forms;

namespace MarconiPieralisi.ElMerendero.WinApp
{
    class ClsCategoria
    {
        //private const string WSPATH = "http://elmerendero.iismarconipieralisi.it/ws2/categorie/";
        private const string WSPATH = Program.WSPATH + "categorie/";

        #region variabili
        private int _id;
        private string _nome;
        private string _imgpath;
        #endregion

        #region proprietà
        public int ID { get => _id; set => _id = value; }
        public string nome { get => _nome; set => _nome = value; }
        public string imgpath { get => _imgpath; set => _imgpath = value; }
        #endregion

        #region costruttore
        public ClsCategoria() { }
        public ClsCategoria(string id, string Nome, string Imgpath)
        {
            ID = Convert.ToInt32(id);
            nome = Nome;
            imgpath = Imgpath;
        }
        #endregion

        #region metodi
        public List<ClsCategoria> getCategorieSENZAGSUITE()
        {
            List<ClsCategoria> libri = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getCategorie.php");
                request.Method = "GET"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    string JsonStr = reader.ReadToEnd();                            // Legge i ch dello stream fino all'ultimo
                    libri = JsonConvert.DeserializeObject<List<ClsCategoria>>(JsonStr);    // Deserializza la stringa JSON in una lista di Libro
                }
                response.Close();
            }
            catch (Exception ex)
            {

            }
            return libri;
        }

        public List<ClsCategoria> getCategorie()
        {
            List<ClsCategoria> categorie = null;

            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "getCategorie.php");
                request.Method = "POST"; // Se dobbiamo inviare dei filtri usiamo la querystring (GET) se invece dobbiamo usare JSON modificare il metodo in POST
                request.ContentType = "application/json";
                request.ContentLength = JsonStr.Length;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                // Creo lo stream della richiesta e aggiungo la stringa JSon
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    categorie = JsonConvert.DeserializeObject<List<ClsCategoria>>(JsonStr);    // Deserializza la stringa JSON in una lista di Libro
                }
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return categorie;
        }

        public string createCategoria()
        {
            string msg = "";

            // Serializzo su stringa JSon l'oggetto libro
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            // Creo la richiesta al WebService

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "postCategoria.php");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = JsonStr.Length;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

            // Creo lo stream della richiesta e aggiungo la stringa JSon
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(JsonStr);
            writer.Flush();

            try
            {
                

                // Leggo la risposta
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.Created) // Codice 201: Created (https://it.wikipedia.org/wiki/Codici_di_stato_HTTP)
                {
                    // Prelevo lo stream della risposta
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte

                    // Leggo e deserializzo la risposta
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                // Chiudo la risposta
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return msg;
        }

        public string deleteCategoria()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "deleteCategoria.php");
                request.Method = "DELETE";
                request.ContentType = "application/json";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return msg;
        }

        public string updateCategoria()
        {
            string msg = "";
            string JsonStr = JsonConvert.SerializeObject(this, Formatting.Indented);
            JsonStr = Program.AggiungiCredenziali(JsonStr);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(WSPATH + "putCategoria.php");
                request.Method = "PUT";
                request.ContentType = "application/json";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"; // Necessario x quei server che verificano l'origine della chiamata

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.Write(JsonStr);
                writer.Flush();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) // 200
                {
                    Stream responseStream = response.GetResponseStream();           // Sequenza di byte ricevuti in risposta
                    StreamReader reader = new StreamReader(responseStream);         // Oggetto reader per leggere i byte
                    JsonStr = reader.ReadToEnd();                                   // Legge i ch dello stream fino all'ultimo
                    var chiave_valore = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);    // Deserializza la stringa JSON in un dictionary Chiave-Valore
                    msg = chiave_valore["message"].ToString();
                }

                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return msg;
        }
        #endregion
    }
}
