﻿namespace MarconiPieralisi.ElMerendero.WinApp
{
    partial class FrmOrdine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOrdine));
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnConfermaOrdine = new System.Windows.Forms.Button();
            this.btnEliminaCarrello = new System.Windows.Forms.Button();
            this.btnAggiungiOrdine = new System.Windows.Forms.Button();
            this.udQuantita = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.lsvprodotti = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label12 = new System.Windows.Forms.Label();
            this.lsvCarrello = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udQuantita)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(248, 7);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(385, 74);
            this.pictureBox3.TabIndex = 14;
            this.pictureBox3.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(266, 451);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(146, 19);
            this.label10.TabIndex = 34;
            this.label10.Text = "Prezzo Ordine: 0€";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(267, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 13);
            this.label11.TabIndex = 134;
            this.label11.Text = "Prodotti della categoria";
            // 
            // btnConfermaOrdine
            // 
            this.btnConfermaOrdine.Enabled = false;
            this.btnConfermaOrdine.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfermaOrdine.Location = new System.Drawing.Point(704, 413);
            this.btnConfermaOrdine.Name = "btnConfermaOrdine";
            this.btnConfermaOrdine.Size = new System.Drawing.Size(160, 35);
            this.btnConfermaOrdine.TabIndex = 133;
            this.btnConfermaOrdine.Text = "Conferma Ordine";
            this.btnConfermaOrdine.UseVisualStyleBackColor = true;
            this.btnConfermaOrdine.Click += new System.EventHandler(this.btnConfermaOrdine_Click);
            // 
            // btnEliminaCarrello
            // 
            this.btnEliminaCarrello.Enabled = false;
            this.btnEliminaCarrello.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminaCarrello.Location = new System.Drawing.Point(704, 359);
            this.btnEliminaCarrello.Name = "btnEliminaCarrello";
            this.btnEliminaCarrello.Size = new System.Drawing.Size(160, 35);
            this.btnEliminaCarrello.TabIndex = 132;
            this.btnEliminaCarrello.Text = "Elimina dal carrello";
            this.btnEliminaCarrello.UseVisualStyleBackColor = true;
            this.btnEliminaCarrello.Click += new System.EventHandler(this.btnEliminaCarrello_Click);
            // 
            // btnAggiungiOrdine
            // 
            this.btnAggiungiOrdine.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAggiungiOrdine.Location = new System.Drawing.Point(704, 225);
            this.btnAggiungiOrdine.Name = "btnAggiungiOrdine";
            this.btnAggiungiOrdine.Size = new System.Drawing.Size(160, 35);
            this.btnAggiungiOrdine.TabIndex = 131;
            this.btnAggiungiOrdine.Text = "Aggiungi all\'ordine";
            this.btnAggiungiOrdine.UseVisualStyleBackColor = true;
            this.btnAggiungiOrdine.Click += new System.EventHandler(this.btnAggiungiOrdine_Click);
            // 
            // udQuantita
            // 
            this.udQuantita.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udQuantita.Location = new System.Drawing.Point(704, 160);
            this.udQuantita.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udQuantita.Name = "udQuantita";
            this.udQuantita.Size = new System.Drawing.Size(75, 20);
            this.udQuantita.TabIndex = 130;
            this.udQuantita.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udQuantita.ValueChanged += new System.EventHandler(this.udQuantita_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(700, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 19);
            this.label9.TabIndex = 129;
            this.label9.Text = "Quantità:";
            // 
            // lsvprodotti
            // 
            this.lsvprodotti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lsvprodotti.FullRowSelect = true;
            this.lsvprodotti.HideSelection = false;
            this.lsvprodotti.Location = new System.Drawing.Point(270, 116);
            this.lsvprodotti.MultiSelect = false;
            this.lsvprodotti.Name = "lsvprodotti";
            this.lsvprodotti.Size = new System.Drawing.Size(406, 144);
            this.lsvprodotti.TabIndex = 128;
            this.lsvprodotti.UseCompatibleStateImageBehavior = false;
            this.lsvprodotti.View = System.Windows.Forms.View.Details;
            this.lsvprodotti.SelectedIndexChanged += new System.EventHandler(this.lsvprodotti_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nome";
            this.columnHeader1.Width = 136;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Prezzo";
            this.columnHeader2.Width = 61;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Quantità";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(267, 288);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 136;
            this.label12.Text = "Carrello";
            // 
            // lsvCarrello
            // 
            this.lsvCarrello.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lsvCarrello.FullRowSelect = true;
            this.lsvCarrello.HideSelection = false;
            this.lsvCarrello.Location = new System.Drawing.Point(270, 304);
            this.lsvCarrello.MultiSelect = false;
            this.lsvCarrello.Name = "lsvCarrello";
            this.lsvCarrello.Size = new System.Drawing.Size(406, 144);
            this.lsvCarrello.TabIndex = 128;
            this.lsvCarrello.UseCompatibleStateImageBehavior = false;
            this.lsvCarrello.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Nome";
            this.columnHeader4.Width = 136;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Prezzo";
            this.columnHeader5.Width = 61;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Quantità";
            // 
            // FrmOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(887, 528);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnConfermaOrdine);
            this.Controls.Add(this.btnEliminaCarrello);
            this.Controls.Add(this.btnAggiungiOrdine);
            this.Controls.Add(this.udQuantita);
            this.Controls.Add(this.lsvCarrello);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lsvprodotti);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmOrdine";
            this.Text = "FrmStudente";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOrdine_FormClosing);
            this.Load += new System.EventHandler(this.FrmUtente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udQuantita)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnConfermaOrdine;
        private System.Windows.Forms.Button btnEliminaCarrello;
        private System.Windows.Forms.Button btnAggiungiOrdine;
        private System.Windows.Forms.NumericUpDown udQuantita;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListView lsvprodotti;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListView lsvCarrello;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}